#Usage: this code only needs to be run once.
# python setup.py develop --user 
#07/04/2015 Created by Nicole Czakon
#
#      packages=['load_data','configuration_files'], \
from setuptools import setup
from setuptools import find_packages

setup(name='load_data', \
      description = 'this package contains the code need to load the data.', \
      author='Nicole Czakon', \
      #packages=find_packages(), \
      packages=['load_data'],\
      #,'configuration_files'], \
      zip_sage=False)


