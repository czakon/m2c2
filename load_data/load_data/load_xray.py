# NAME: load_xray
# 
# PURPOSE:
# To make a uniform package to load all of my different xray files
#
# USAGE: from load_data import load_xray
#        mantz2010 = load_xray.mantz2010()
#
# MODIFICATION HISTORY:
# 05/08/2015 NGC: Created
#########################################################################
# STANDARD PYTHON PACKAGES
from code import interact
from copy import deepcopy

# EXTERNAL PACKAGES
import numpy as np
import matplotlib.pyplot as plt
import fitsio
from astropy.io import ascii
from astropy.table import join,vstack
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.cosmology import FlatLambdaCDM

# M2C2 Packages
from load_data.configuration_files import cosmo_config
from load_data.configuration_files import xray_files_config
from cluster_database.configuration_files.sample_cluster_json \
    import cluster_json,cluster_measurement
from cluster_database.configuration_files.sample_cluster_json \
    import basic_measurement as cMeas

# Global Variables
global cosmo
cosmo = FlatLambdaCDM(H0=cosmo_config.h0*100,Om0=cosmo_config.Omega_M)

def mcxc():

    filename = xray_files_config.mcxc
    m_x = fitsio.read(filename)

    m_x_all = []
    catalog = "mcxc"

    for j in range(len(m_x)):

        # Need to make a deepcopy, otherwise, cluster_json will be
        # treated as a pointer.
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = deepcopy(cluster_measurement)
        cj = cluster_j[catalog]
        cj["cluster_id"] = m_x[j]["NAME"]
        cj["alt_cluster_id"] = m_x[j]["ALT_NAME"]
        cj["redshift"]["bf"] = float(m_x[j]["REDSHIFT"])
        cj["ra"] = float(m_x[j]["RA"])#degrees
        cj["decl"] = float(m_x[j]["DEC"])#degrees
        cj["mass"] = {"500":deepcopy(cMeas)}
        #convert to 1E14/h SOLAR_MASS
        cj["mass"]["500"]["bf"] = float(m_x[j]["MASS_500"]*1E-14*cosmo.h)
        cj["radius"] = {"500":deepcopy(cMeas)} #MPC
        cj["radius"]["500"]["bf"] = float(m_x[j]["RADIUS_500"])
        cj["lx"] = {"500":deepcopy(cMeas)}
        cj["lx"]["500"]["bf"] = float(m_x[j]["LX_500"]*1E-44) #ERG/S
        m_x_all.append(cluster_j)
        
    return m_x_all

def bcs():

    filename = xray_files_config.bcs    
    bcs_data = ascii.read(filename,data_start=6,data_end=212,
                          format="fixed_width",header_start=5,
                          delimiter=" ")
    bcs_all = []
    catalog = "bcs"

    for b_x in bcs_data:

        # Need to make a deepcopy, otherwise, cluster_json will be
        # treated as a pointer.
        cj = deepcopy(cluster_measurement)
        
        cj["cluster_id"] = b_x["____name_______"]
        cj["redshift"]["bf"] = b_x["___z___"]
        cj["ra"] = b_x["__R.A._"]#degrees
        cj["decl"] = b_x["__Dec_"]#degrees
        cj["tx"] = {"full":deepcopy(cMeas)}
        cj["tx"]["full"]["bf"] = b_x["_kT__"]
        cj["lx"] = {"full":deepcopy(cMeas)}        
        cj["lx"]["full"]["bf"] = b_x["lx(44)"]
        #cj["radius"]["bf"] = row[7] #r_VTP #arcmin
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj
        bcs_all.append(cluster_j)

    return bcs_all

def ebcs():

    filename = xray_files_config.ebcs    
    bcs_data = ascii.read(filename,data_start=6,data_end=113,
                          format="fixed_width",header_start=5,
                          delimiter=" ")
    bcs_all = []
    catalog = "ebcs"

    for b_x in bcs_data:

        # Need to make a deepcopy, otherwise, cluster_json will be treated
        # as a pointer.
        cj = deepcopy(cluster_measurement)
        #cj = cluster_j[catalog]
        cj["cluster_id"] = b_x["____name_______"]
        cj["redshift"]["bf"] = b_x["___z___"]
        cj["ra"] = b_x["__R.A._"]#degrees
        cj["decl"] = b_x["__Dec_"]#degrees
        cj["tx"] = {"full":deepcopy(cMeas)}
        cj["tx"]["full"]["bf"] = b_x["_kT__"]
        cj["lx"] = {"full":deepcopy(cMeas)}
        cj["lx"]["full"]["bf"] = b_x["lx(44)"]
        #cj["radius"]["bf"] = row[7] #r_VTP #arcmin
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj

        bcs_all.append(cluster_j)

    return bcs_all

def reflex():

    catalog = "reflex"
    filename = xray_files_config.reflex
    reflex_data = ascii.read(filename)
    reflex_all = []

    for row in reflex_data:
        
	temp_coord = SkyCoord(row["RA"]+" "+row["DEC"],
                              unit=(u.hourangle,u.deg))
        
        cj = deepcopy(cluster_measurement)        
        cj["cluster_id"] = row["cluster_id"]
        cj["redshift"]["bf"] = float(row["z"])
        cj["ra"] =  temp_coord.ra.deg#row["RA"])
        cj["decl"] =  temp_coord.dec.deg#row["DEC"])
        cj["lx"] = {"500":deepcopy(cMeas)}
        #which radius??
        cj["lx"]["500"]["bf"] = float(row["lx"]*1E-44)  
        #cj["radius"]["bf"] = row[7] #r_VTP #arcmin
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj
        
        reflex_all.append(cluster_j)
        
    return reflex_all

def mantz2010():

    catalog = "mantz2010"
    # RA DEC and the cluster X-ray measurements are in different
    # tables/files.
    filename = xray_files_config.mantz2010_data
    m10_data = ascii.read(filename)
    filename = xray_files_config.mantz2010_radec
    m10_radec = ascii.read(filename)
    mantz_data = join(m10_data,m10_radec)
    mantz_all = []
    print(str(len(mantz_data)) + \
          " clusters in the Mantz et al. (2010) sample....")  

    for row in mantz_data:

        temp_coord = SkyCoord(row["RA_J2000"]+" "+row["Dec_J2000"], \
                              unit=(u.hourangle,u.deg))

        cj = deepcopy(cluster_measurement)
        cj["cluster_id"] = row["cluster_id"]
        cj["redshift"]["bf"] = float(row["redshift"])
        cj["ra"] = temp_coord.ra.deg
        cj["decl"] = temp_coord.dec.deg
        
        cj["c"] = {"200":deepcopy(cMeas)}
        cj["c"]["200"]["bf"] = 4.0
        cj["radius"] = {"500":deepcopy(cMeas)}
        cj["radius"]["500"]["bf"] = float(row["r500"])
        cj["radius"]["500"]["sig"]["median"] = float(row["r500_err"])
        cj["mass"] = {"500":deepcopy(cMeas)}
        cj["mass"]["500"]["bf"] = float(row["M500"]*cosmo.h)
        cj["mass"]["500"]["sig"]["median"] = float(row["M500_err"] \
                                                   * cosmo.h)
        cj["mgas"] = {"500":deepcopy(cMeas)}
        #cj["mgas"]["matching_catalog"] = catalog
        cj["mgas"]["500"]["bf"] = float(row["Mgas500"]*cosmo.h)
        cj["mgas"]["500"]["sig"]["median"] = float(row["Mgas500_err"] \
                                                   * cosmo.h)
        #cj["lx"]["matching_catalog"] = catalog
        cj["lx"] = {"500":deepcopy(cMeas)}
        cj["lx"]["500"]["bf"] = float(row["L500"])
        cj["lx"]["500"]["sig"]["median"] = float(row["L500_err"])
        cj["lx_ce"] = {"500":deepcopy(cMeas)}
        #cj["lx_ce"]["matching_catalog"] = catalog
        cj["lx_ce"]["500"]["bf"] = float(row["L500ce"])
        cj["lx_ce"]["500"]["sig"]["median"] = float(row["L500ce_err"])
        cj["tx"] = {"500":deepcopy(cMeas)}
        #cj["tx"]["matching_catalog"] = catalog
        cj["tx"]["500"]["bf"] = float(row["kTce"])
        cj["tx"]["500"]["sig"]["median"] = float(row["kTce_err"])

	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj
        mantz_all.append(cluster_j)

    return mantz_all

def mahdavi2013():

    catalog = "mahdavi2013"
    #RA DEC and the cluster measurements are in different tables/files.
    filename = xray_files_config.mahdavi2013_tbl1
    m13_tbl1 = ascii.read(filename)

    filename = xray_files_config.mahdavi2013_tbl2
    m13_tbl2 = ascii.read(filename)

    mahdavi_data = join(m13_tbl1,m13_tbl2)
    mahdavi_all = []
    
    for row in mahdavi_data:

        temp_coord = SkyCoord(row["RA_J2000"]+" "+row["Dec_J2000"],\
                              unit=(u.hourangle,u.deg))
        cj = deepcopy(cluster_measurement)        
        #cj["matching_catalog"] = catalog
        #cj["matching_id"] = row["cluster_id"]
        cj["cluster_id"] = row["cluster_id"]
        #cluster_j["nominal"]["cluster_id"] = row["cluster_id"]
        cj["redshift"]["bf"] = float(row["redshift"])
        cj["ra"] = temp_coord.ra.deg
        cj["decl"] = temp_coord.dec.deg
        cj["radius"] = {"500":deepcopy(cMeas)}
        cj["radius"]["500"]["bf"] = float(row["r500wl"])
        cj["radius"]["500"]["sig"]["median"] = float(row["r500wl_err"])
        cj["mass"] = {"500":deepcopy(cMeas)}
        cj["mass"]["500"]["bf"] = float(row["Mwl"]*cosmo.h)
        cj["mass"]["500"]["sig"]["median"] = float(row["Mwl_err"]*cosmo.h)
        cj["mgas"] = {"500":deepcopy(cMeas)}
        #cj["mgas"]["matching_catalog"] = catalog
        cj["mgas"]["500"]["bf"] = float(row["Mgas"]*cosmo.h)
        cj["mgas"]["500"]["sig"]["median"] = float(row["Mgas_err"]\
                                                   *cosmo.h)
        cj["mhydro"] = {"500":deepcopy(cMeas)}
        cj["mhydro"]["500"]["bf"] = float(row["Mhydro"]*cosmo.h)
        cj["mhydro"]["500"]["sig"]["median"] = float(row["Mhydro_err"]\
                                                     *cosmo.h)
        cj["lx"] = {"500":deepcopy(cMeas)}
        #cj["lx"]["matching_catalog"] = catalog
        #1E44 erg/s
        cj["lx"]["500"]["bf"] = float(row["Lx500"]*0.10)
        cj["lx"]["500"]["sig"]["median"] = float(row["Lx500_err"]*0.10)
        cj["tx"] = {"500":deepcopy(cMeas)}
        #cj["tx"]["matching_catalog"] = catalog
        cj["tx"]["500"]["bf"] = float(row["Tx500"])#keV
        cj["tx"]["500"]["sig"]["median"] = float(row["Tx500_err"])#keV
        
        # This is obsolete code, get rid of it once it"s determined not
        # to be  useful.
        #if "mgas" not in cluster_j["nominal"].keys():
        #    cluster_j["nominal"]["mgas"] = deepcopy(cj["mgas"])
        #    cluster_j["nominal"]["mgas"]["matching_catalog"] = catalog

        #if "tx" not in cluster_j["nominal"].keys():
        #    cluster_j["nominal"]["tx"] = deepcopy(cj["tx"])
        #    cluster_j["nominal"]["tx"]["matching_catalog"] = catalog

        #if "lx" not in cluster_j["nominal"].keys():
        #    cluster_j["nominal"]["lx"] = deepcopy(cj["lx"])
        #    cluster_j["nominal"]["lx"]["matching_catalog"] = catalog
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj

        mahdavi_all.append(cluster_j)
        
    return mahdavi_all

def landry2013():

    catalog = "landry2013"
    filename = xray_files_config.landry2013_masses
    l13_masses = ascii.read(filename)
    filename = xray_files_config.landry2013_z
    l13_z = ascii.read(filename)
    landry_data = join(l13_masses,l13_z)
    landry_all = []
    print(str(len(landry_data)) + \
          " clusters in the Landry et al. (2013) sample....")
    
    for row in landry_data:

        cj = deepcopy(cluster_measurement)
        #cj["matching_catalog"] = catalog
        #cj["matching_id"] = row["cluster_id"]
        cj["cluster_id"] = row["cluster_id"]
        #cluster_j["nominal"]["cluster_id"] = row["cluster_id"]
        cj["redshift"]["bf"] = float(row["redshift"])
        cj["ra"] = float(row["RA_J2000"])
        cj["decl"] = float(row["Dec_J2000"])

        cj["radius"] = {"500":deepcopy(cMeas)}
        cj["radius"]["500"]["bf"] = float(row["r500"])
        cj["radius"]["500"]["sig"]["upper"] = float(row["r500_upper"])
        cj["radius"]["500"]["sig"]["lower"] = \
            float(np.abs(row["r500_lower"]))
        cj["radius"]["500"]["sig"]["median"] = \
            float(0.5*(row["r500_upper"] + np.abs(row["r500_lower"])))
        cj["radius"] = {"2500":deepcopy(cMeas)}
        cj["radius"]["2500"]["bf"] = float(row["r2500"])
        cj["radius"]["2500"]["sig"]["upper"] = float(row["r2500_upper"])
        cj["radius"]["2500"]["sig"]["lower"] = \
            float(np.abs(row["r2500_lower"]))
        cj["radius"]["2500"]["sig"]["median"] = \
            float(0.5*(row["r2500_upper"] + np.abs(row["r2500_lower"])))
        #cj["mgas"]["matching_catalog"] = catalog
        cj["mgas"] = {"500":deepcopy(cMeas)}
        cj["mgas"]["500"]["bf"] = float(row["Mgas500"]*cosmo.h)
        cj["mgas"]["500"]["sig"]["upper"] = \
            float(row["Mgas500_upper"]*cosmo.h)
        cj["mgas"]["500"]["sig"]["lower"] = \
            float(np.abs(row["Mgas500_lower"])*cosmo.h)
        cj["mgas"]["500"]["sig"]["median"] = \
            float(0.5*(row["Mgas500_upper"] + \
                       np.abs(row["Mgas500_lower"]))*cosmo.h)
        cj["mgas"] = {"2500":deepcopy(cMeas)}        
        cj["mgas"]["2500"]["bf"] = float(row["Mgas2500"]*cosmo.h)
        cj["mgas"]["2500"]["sig"]["upper"] = \
            float(row["Mgas2500_upper"]*cosmo.h)
        cj["mgas"]["2500"]["sig"]["lower"] = \
            float(np.abs(row["Mgas2500_lower"])*cosmo.h)
        cj["mgas"]["2500"]["sig"]["median"] = \
            float(0.5*(row["Mgas2500_upper"] \
                       + np.abs(row["Mgas2500_lower"]))*cosmo.h)
        #These are mhydro measurements...
        cj["mass"] = {"500":deepcopy(cMeas)}
        cj["mass"]["500"]["bf"] = float(row["Mtot500"]*cosmo.h)
        cj["mass"]["500"]["sig"]["upper"] = \
            float(row["Mtot500_upper"]*cosmo.h)
        cj["mass"]["500"]["sig"]["lower"] = \
            float(np.abs(row["Mtot500_lower"])*cosmo.h)
        cj["mass"]["500"]["sig"]["median"] = \
            float(0.5*(row["Mtot500_upper"] \
                       + np.abs(row["Mtot500_lower"]))*cosmo.h)
        cj["mass"] = {"2500":deepcopy(cMeas)}
        cj["mass"]["2500"]["bf"] = float(row["Mtot2500"]*cosmo.h)
        cj["mass"]["2500"]["sig"]["upper"] = \
            float(row["Mtot2500_upper"]*cosmo.h)
        cj["mass"]["2500"]["sig"]["lower"] = \
            float(np.abs(row["Mtot2500_lower"])*cosmo.h)
        cj["mass"]["2500"]["sig"]["median"] = \
            float(0.5*(row["Mtot2500_upper"] \
                       + np.abs(row["Mtot2500_lower"]))*cosmo.h)
        #This data is in the table, but hasn"t been incorporated yet
        #cj["lx"]["500"]["bf"] = float(row["Lx500"]*0.10)#1E44 erg/s
        #1E44 erg/s
        #cj["lx"]["500"]["sig"]["median"] = float(row["Lx500_err"]*0.10)
        #cj["tx"]["500"]["bf"] = float(row["Tx500"])#keV
        #cj["tx"]["500"]["sig"]["median"] = float(row["Tx500_err"])#keV
        #cluster_j["nominal"]["lx"] = deepcopy(cj["lx"])
        #cluster_j["nominal"]["tx"] = deepcopy(cj["tx"])
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj

        landry_all.append(cluster_j)
    return landry_all

def vikhlinin2009():

    catalog = "vikhlinin2009"
    filename = xray_files_config.vikhlinin2009_lowz
    v09_lz = ascii.read(filename)
    print(str(len(v09_lz)) + \
          " clusters in the Vikhlinin, 2009, low redshift sample.")
    filename = xray_files_config.vikhlinin2009_highz
    v09_hz = ascii.read(filename,guess=False)
    print(str(len(v09_hz)) + \
          " clusters in the Vikhlinin, 2009, high redshift sample.")
    filename = xray_files_config.four00dmain
    four00d = ascii.read(filename)
    print(str(len(four00d)) + " clusters in the 400d sample.")
    vikhlinin_data = join(v09_hz,four00d)
    print(str(len(vikhlinin_data)) + \
          " clusters in common with the high redshift sample and the"
          " 400d sample.")
    vikhlinin_data = vstack([vikhlinin_data,v09_lz])
    print(str(len(vikhlinin_data)) + \
          " clusters found after merging the high and low redshift" \
          + " samples.")
    v09_all = []
    
    for row in vikhlinin_data:

        temp_coord = SkyCoord(
            row["RA_J2000"]+" "+row["Dec_J2000"],
            unit=(u.hourangle,u.deg))
        cj = deepcopy(cluster_measurement)
        cj["cluster_id"] = row["cluster_id"]
        cj["redshift"]["bf"] = float(row["redshift"])
        cj["ra"] = temp_coord.ra.deg
        cj["decl"] = temp_coord.dec.deg

        cj["lx"] = {"500":deepcopy(cMeas)}
        cj["lx"]["500"]["bf"] = float(row["Lx"])#1E44 erg/s

        cj["tx"] = {"500":deepcopy(cMeas)}
        cj["tx"]["500"]["bf"] = float(row["Tx"])
        cj["tx"]["500"]["sig"]["median"] = float(row["Txerr"])

        cj["mass"] = {"500":deepcopy(cMeas)}
        cj["mass"]["500"]["bf"] = float(row["My"]*cosmo.h)
        cj["mass"]["500"]["sig"]["upper"] = float(row["Myerr"]*cosmo.h)
        cj["mass"]["500"]["sig"]["lower"] = float(np.abs(row["Myerr"]) \
                                                  *cosmo.h)
        cj["mass"]["500"]["sig"]["median"] = float(row["Myerr"]*cosmo.h)
        #This must be multiplied by the fg-M relation in Vikhlinin 2009
        # (I think).
        #Have to convert the fg to r
        fg = (0.72/cosmo.h)**1.5*(0.125 +0.0377*float(row["Mg"]*0.1))
        #cj["mgas"]["matching_catalog"] = catalog
        cj["mgas"] = {"500":deepcopy(cMeas)}
        cj["mgas"]["500"]["bf"] = fg*float(row["Mg"]*cosmo.h)

        # This is obsolete code, get rid of it once you determine that
        # it"s  no longer of use.
        # I"m too lazy to figure out what the true error in the mg values
        # is...
        #cj["mg"]["500"]["sig"]["median"] = float(row["My_err"])
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj
        v09_all.append(cluster_j)
        
    return v09_all

def donahue2015():

    filename = xray_files_config.donahue2015Table1
    donahue_data = ascii.read(filename,data_start=3,data_end=28,
                              header_start=2)
    donahue_all = []

    catalog = "donahue2015"
    print(str(len(donahue_data)) + \
          " clusters in the Donahue et al. (2015) sample....")        
    for row in donahue_data:

        # Need to make a deepcopy, otherwise, cluster_json will be
        # treated as a pointer.
        cj = deepcopy(cluster_measurement)
        cj["cluster_id"] = row["Name"]
        degreeCoord = SkyCoord(row["R.A.(J2000)"]+" "+row["Decl.(J2000)"],
                               unit=(u.hourangle,u.deg))
        cj["ra"] =  degreeCoord.ra.deg
        cj["decl"] =  degreeCoord.dec.deg
        # This is a new cluster entry added in order to take a look at the
        # difference between relaxed and unrelaxed galaxy clusters
        cj["K0"] = row["K_0"]
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj
        
        donahue_all.append(cluster_j)

    return donahue_all

if __name__ == "__main__":
    n=0
    for i in (mcxc()):
        if n>1499:
            print i
        n+=1
