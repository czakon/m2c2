#!/usr/bin/env python2.7
# NAME: load_richness
# 
# PURPOSE:
# To make a uniform package to load all of my different richness results
# 
# Types of files:
# andreon2016.txt
#
# USAGE:
#
# MODIFICATION HISTORY:
# 04/25/2016 NGC Created
###########################################################################################
# General Python Code
from copy import deepcopy
from code import interact
# Special Python Packages
from astropy.io import ascii
from astropy.cosmology import FlatLambdaCDM
# My own code
from load_data.configuration_files import cosmo_config
from load_data.configuration_files import richness_files_config
from cluster_database.configuration_files.sample_cluster_json import cluster_json
# Define the global parameters
global cosmo
cosmo = FlatLambdaCDM(H0=cosmo_config.h0*100,Om0=cosmo_config.Omega_M)

def andreon2016():

    filename = richness_files_config.andreon2016
    andreon_ascii = ascii.read(filename)
    andreon_data = []
    catalog = 'andreon2016'
    
    for row in andreon_ascii:

        #Need to make a deepcopy, otherwise, cluster_json will be treated as a pointer.
        cluster_j = deepcopy(cluster_json)
        
        cluster_j[catalog] = {}
        cluster_j[catalog]['cluster_id'] = row['ID']
        if row['NED~ID']=='NaN':

            if row['Piffaretti~et~al.~2011~ID']=='NaN':

                if row['Abell~1958~ID']=='NaN':
                    
                    if row['Planck~2015~ID']=='NaN':
                        alt_cluster_id = row['ID']
                    else:
                        alt_cluster_id = row['Planck~2015~ID'].split('~')[0]
                else:
                    alt_cluster_id = row['Abell~1958~ID'].split('~')[0]
            else:
                alt_cluster_id = row['Piffaretti~et~al.~2011~ID'].split('~')[0]
        else:
            alt_cluster_id = row['NED~ID'].split('~')[0]
        cluster_j[catalog]['alt_cluster_id'] = alt_cluster_id
        cluster_j[catalog]['ra'] = row['R.A.']
        cluster_j[catalog]['decl'] = row['Dec.']
        cluster_j[catalog]['redshift'] = {}
        cluster_j[catalog]['redshift']['bf'] = row['z']
        cluster_j[catalog]['mass'] = {}
        cluster_j[catalog]['mass']['200'] = {}
        cluster_j[catalog]['mass']['200']['bf'] = 10**(row['z']-14.0)*cosmo.h#1E14/h SOLAR_MASSa
        andreon_data.append(cluster_j)

    return andreon_data


if __name__ == "__main__":
    x = andreon2016()
    i=0
    for row in x:
        i+=1
        print('\nThis is a sample row for andreon2016.\n')
        print row['andreon2016']
        if i==1:
            break


