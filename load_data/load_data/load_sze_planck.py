# NAME: load_sze_planck
# 
# PURPOSE:
# To compare various SZE studies to the planck cluster catalog
# 
# Types of files:
#
# USAGE:
#
# MODIFICATION HISTORY:
# 02/17/2015 NGC: Created
####################################################################################
# Things to do for this code.
# 1. Find what the conversion is from Y2500 to Y500
# 2. Figure out what DA is and multiply it by the relevant fraction to get the right thing.
# 3. Find out what we think our apeture bias is for these measurements.
# 4. Make a plot comparing the different mass measurements.
import matplotlib as mpl
from matplotlib import rc
from matplotlib.ticker import FormatStrFormatter
import pylab as pl
import numpy as np
from numpy import array
from astropy.table import Table
from astropy.io import fits
from astropy.io import ascii
from code import interact
import sze_files_config
import mass_files_config
from scipy.io import readsav
###
import astropy.coordinates as coord
import astropy.units as units
import pickle
import os
#
#def find_cat_x(catA,catB):
#   Purpose:This functions compares to catalogs and returns the intersection of those catalogs 
#       based on their viscinity in RA and Dec.
#   Usage: closest_cluster = find_cat_x(catA,catB)
def find_cat_x(catA,catB):
    #Redefines catA to be the smaller of the two catalogs.
    if len(catB) < len(catA): 
        temp = catA
        catA = catB
        catB = temp
    #Since cluster names are not consistent between catalogs, we'll say that they're the
    #same cluster if their centers are within 10 arcmin of each other.
    closest_cluster = [60.0*np.min(np.sqrt(np.abs((np.float64(catA["ra"][i])-catB["ra"])**2+ \
                                                  (np.float64(catA["dec"][i])-catB["dec"])**2))) \
                       for i in range(len(catA))]
    good_catA = np.where(np.array(closest_cluster) < 10.0)

    catA = catA[good_catA]

    # We still want to keep catB because it contains auxiliary information for the cluster that
    # is not found in catA. Repeat the above step to ensure that catB is in the same order as catA. 
    good_catB = [np.argmin(np.sqrt(np.abs((np.float64(catA["ra"][i])-catB["ra"])**2+ \
                                          (np.float64(catA["dec"][i])-catB["dec"])**2))) \
                 for i in range(len(catA))]

    cat_x = catB[good_catB]
    #Return the intersection of the catalog
    return [catA,cat_x]
########################################################################
# Start of the main program
########################################################################
#Get the name of the Mantz, 2010 data file from the configuration parameters.
filename = mass_files_config.mantz2010
# These are the expected column names for this dataset.
# Name,z,RA hms, DEC,m,s,M500
m10_x = ascii.read(filename,delimiter="\t")
temp = np.float64(coord.Angle(m10_x["RA hms"],unit=units.hour).deg)
m10_x.add_column(Table.Column(temp,name='ra'),index=2)
temp = [np.float64(coord.Angle((m10_x["DEC"][j],m10_x["m"][j],m10_x["s"][j]),unit=units.deg)) \
        for j in range(len(m10_x))]
m10_x.add_column(Table.Column(temp,name='dec'),index=2)
########################################################################
#Get the filname for the combined data for all the weak lensing masses.
filename = mass_files_config.comalit
w_x = ascii.read(filename)
#These are the column names for this file
#Name			       ra	     dec	   z	    MatchCODE Name_NED		       RA_NED	    DEC_NED	 z_NED	   AuthorCODE	         BIBCODE	         M2500	 Delta_M2500 M500 Delta_M500 M200 Delta_M200 MVir Delta_MVir
#data_start=2)#,names=[Name	ra	dec	z	MatchCODE	Name_NED	RA_NED	DEC_NED	z_NED	AuthorCODE	BIBCODE	M2500	Delta_M2500 M500 Delta_M500 M200 Delta_M200 MVir Delta_MVir])
w_x["ra"] = coord.Angle(w_x["ra"],unit=units.hour).deg#)np.float64(
w_x["dec"] = coord.Angle(w_x["dec"],unit=units.deg)#)np.float64(

########################################################################
filename = mass_files_config.hst2015
hst_x = ascii.read(filename)
########################################################################
filename = sze_files_config.planck_hst
p_hst = ascii.read(filename)
########################################################################
filename = sze_files_config.planck_mcmc
p_mcmc = fits.open(filename)

ts_min = p_mcmc[1].data["TS_MIN"]
ts_max = p_mcmc[1].data["TS_MAX"]
y_min = p_mcmc[1].data["Y_MIN"]
y_max = p_mcmc[1].data["Y_MAX"]
p_mcmc_temp = p_mcmc[2].data#1271x256x256 source x thetas x y5r500

filename = sze_files_config.planck_dr2
p_dr2 = fits.open(filename)
p_x = p_dr2[1].data

p_x = p_x[np.squeeze(np.argwhere(p_x["MSZ"]!=0))]


filename = sze_files_config.mcxc
#save: Table.read(filename,format='fits')
mcxc = fits.open(filename)
m_x = mcxc[1].data
m_x["MASS_500"] = m_x["MASS_500"]*1E-14

bolo_pickle = 'bolo_py.txt'
if not os.path.exists(bolo_pickle):
    ########################################################################
    #class(def) bolocam:
    filename = sze_files_config.bolocam_sav
    bolocam = readsav(filename,python_dict=True)
    #Sort on redshift
    ymx = bolocam["ymx"]
    ymx.sort(order='REDSHIFT')
    filename = sze_files_config.bolo_centroid
    #BolocamID, RA dec
    b_x = Table.read(filename,format='ascii')
    b_x["ra"] = np.float64(coord.Angle(b_x["ra"],unit=units.hour).deg)
    b_x["dec"] = np.float64(coord.Angle(b_x["dec"],unit=units.deg))

    #This file does not have the redshift information, 
    #so, we will have to sort based on the names
    bolo_id = b_x["BolocamID"]
    bolo_id = [bolo_id[i].replace('_bolocam','') for i in range(len(bolo_id))]
    bolo_id = [bolo_id[i].replace('_sr','') for i in range(len(bolo_id))]
    b_x["BolocamID"] = bolo_id
    #Add these values
    #ymx_src = list(ymx["src"])
    ymx_src = ymx["src"]
    ymx_src = [ymx_src[i].replace('_sr','') for i in range(len(ymx_src))]
    ymx["src"] = ymx_src
    bad_bolo = list(set(b_x["BolocamID"]).difference(set(ymx["src"])))
    #for i in range(len(b_x)):
    tlen = len(bad_bolo)
    for i in range(tlen):
        b_x = np.delete(b_x,(np.where(b_x["BolocamID"]==bad_bolo[i])))

    b_x = Table(b_x)
    b_x["M2500"] = [ymx["mtot"][i]["r500"][0]["norm"][0]["med"][0] for i in range(45)]
#    b_x["Y2500"] = [ymx["y"][i]["r2500"][0]["norm"][0]["med"][0] for i in range(45)]
    b_x["Y2500"] = [10**(ymx["y"][i]["r2500"][0]["log"][0]["med"][0]) for i in range(45)]
    b_x["REDSHIFT"] = ymx["REDSHIFT"]

#    bolo_m = [ymx["mtot"][i]["r500"][0]["norm"][0]["med"][0] for i in range(45)]
#    bolo_y = [ymx["y"][i]["r2500"][0]["norm"][0]["med"][0] for i in range(45)]
#    bolo_z = ymx["REDSHIFT"]
    #bad_bolo = list(set(b_x["BolocamID"]).difference(set(ymx["src"])))
    #r2500.log.med
    #return bolocam
    #Find where the Planck clusters match
    #good_planckx = np.zeros(len(b_x))
 #   print("Finding bolo/p_x intersection.")
 #   mb_x = find_cat_x(b_x,m_x)
    print("Finding p_x/m10_x intersection.")
    [m10p_x,pm10_x] = find_cat_x(p_x,m10_x)
    print("Finding p_x/wl intersection.")
    [wp_x,pw_x] = find_cat_x(p_x,w_x)
    print("Finding p_x/bolo intersection.")
    [bp_x,pb_x] = find_cat_x(p_x,b_x)


#    print("Finding bolo/p_x intersection.")
#    bp_x = find_cat_x(b_x,p_x)
#    print("Finding bolo/p_x/mcxc intersection.")
#    mpb_x = find_cat_x(b_x,mp_x)
    file = open(bolo_pickle,'w')
#    pickle.dump([mp_x,mb_x,pb_x,mpb_x,bolocam,bolo_z,bolo_m,bolo_y],file)
#    [m10p_x,pm10_x] = find_cat_x(p_x,m10_x)
    pickle.dump([pb_x,bp_x,wp_x,pw_x,m10p_x,pm10_x,b_x],file)
    file.close()
else:
    file = open(bolo_pickle,'r')
    pb_x,bp_x,wp_x,pw_x,m10p_x,pm10_x,b_x = pickle.load(file)
#    mp_x,mb_x,pb_x,mpb_x,bolocam,bolo_z,bolo_m,bolo_y = pickle.load(file)
    file.close()
#print len(mp_x)#440
#print len(mp_x)#
#print len(mb_x)#42
print len(pb_x)#31
[hstp_x,phst_x] = find_cat_x(p_x,p_hst)
[hstb_x,bhst_x] = find_cat_x(pb_x,phst_x)
print len(bhst_x)

#print len(mpb_x)#29
#pl.scatter(mp_x["REDSHIFT"],mp_x["MASS_500"],color='black')
fig = pl.figure(0)
#params = {'text.usetex':True}#,'font.sans-serif':['Helvetica']}
#pl.rcParams.update(params)#('text',usetex=True)
#pl.rcParams['text.usetex']=True
pl.scatter(p_x["REDSHIFT"],p_x["MSZ"],color='black',s=10,label=r'PLANCK',facecolors=None)
#pl.scatter(mp_x["REDSHIFT"],mp_x["MSZ"],color='black',s=10,label=r'MCXC$\times$ PLANCK')
#pl.scatter(mb_x["REDSHIFT"],mb_x["MSZ"],color='red',s=40,facecolor='none',linewidth=2,\
#           label=r'MCXC$\times$ BOLOCAM')
pl.scatter(pb_x["REDSHIFT"],pb_x["MSZ"],color='green',s=40,facecolor='none',linewidth=2,\
           label=r'PLANCK$\times$ BOLO')
#pl.scatter(b_x["REDSHIFT"],bolo_m,color='blue',s=40,facecolor='none',linewidth=2,label='BOLOCAM')

pl.legend(loc='lower right',scatterpoints=1)#['MCXC','MCXC$\times$BOLO'
pl.gca().set_yscale('log')
pl.xlim([0.0,1.0])
pl.ylim([0.8,30.0])
fsize=20
pl.xlabel('Redshift',fontsize=fsize)
pl.ylabel('$M_{500}|L_{500}\ [10^{14}M_{\odot}]$ (Arnaud2010) ',fontsize=fsize)
pl.savefig('~/plots/planck_bolo_plots/planck_m_z.eps',format='eps')
########################################################################
# PLOT 2: Y v Redshift
########################################################################
fig = pl.figure(2)
pl.gca().set_yscale('log')
pl.xlim([0.0,1.0])
pl.ylim([1E-4,0.140])
#dr2_y500 = dr2_mcmc[1].data["Y5R500"]*dr2_mcmc[0].header['pp_y2yfh']
p_y500 = p_x["Y5R500"]*p_dr2[0].header['pp_y2yfh']*10**(-3)
pb_y500 = pb_x["Y5R500"]*p_dr2[0].header['pp_y2yfh']*10**(-3)

pl.scatter(p_x["REDSHIFT"],p_y500,color='black',s=10,label=r'PLANCK $Y_{500}$')
pl.scatter(pb_x["REDSHIFT"],pb_y500,color='red',s=40,facecolor='none',label=r'PLANCK X BOLO $Y_{500}$')
pl.scatter(b_x["REDSHIFT"],10*b_x["Y2500"],color='blue',s=40,facecolor='none',linewidth=2,label=r'BOLOCAM $10Y_{2500}$')
pl.xlabel('Redshift',fontsize=fsize)
pl.ylabel('$Y_{SZ}$',fontsize=fsize)
pl.legend(loc='upper right',scatterpoints=1)#['MCXC','MCXC$\times$BOLO'
pl.savefig('planck_y_z.eps',format='eps')
########################################################################
# PLOT 3: Mass comparison
# Take the ratio of the bolocam plots with the rest ofthe stuff.
#########################################################################
fig = pl.figure(1)
pl.gca().set_xscale('log')
#pl.gca().set_yscale('log')
#pl.xlim([0.0,1.0])
#pl.ylim([0.1,140.0])
tempb = [bp_x["Y2500"][j]/pb_y500[j] for j in range(len(pb_y500))]
#pl.scatter(bp_x["Y2500"],bp_x["Y2500"]/pb_y500,color='black',s=10,label=r'PLANCK $Y_{500}$')
pl.scatter(bp_x["Y2500"],tempb,color='black',s=10,label=r'PLANCK $Y_{500}$')
pl.xlabel('$Y_{2500}$ (Bolocam)',fontsize=fsize)
pl.ylabel('$Y_{2500}$ (Bolocam)/$Y_{500}$ (Planck)',fontsize=fsize)
#pl.legend(loc='upper right',scatterpoints=1)#['MCXC','MCXC$\times$BOLO'
pl.savefig('planck_v_bolo_y.eps',format='eps')
#########################################################################
fig = pl.figure(3)
wp_x_sort = wp_x#;.group_by('AuthorCODE')'gruen+14'~10
WtG = np.argwhere(wp_x_sort['AuthorCODE']=='applegate+14')
JACO = np.argwhere(wp_x_sort['AuthorCODE']=='mahdavi+13')
Oguri = np.argwhere(wp_x_sort['AuthorCODE']=='oguri+12')
Umetsu = np.argwhere(wp_x_sort['AuthorCODE']=='umetsu+14')
Cypriano = np.argwhere(wp_x_sort['AuthorCODE']=='cypriano+04')
Gruen = np.argwhere(wp_x_sort['AuthorCODE']=='gruen+14')
cAll = 'gray'
cWtG = 'blue'#'red'
#cMantz = 'green'
cbf = 'blue'#'red'#
cone = 'black'#'purple'#'blue'#'brown'#
cPlanck = 'magenta'
lHST = 'RELICS'
lWtG = 'WtG'
lWtGbf = 'WtG best-fit'
lone = '1:1'
lCMB = '43% underestimate'
cHST = 'red'#'purple'#'blue'#
ceHST = 'red'#'purple'#'blue'#
#cJACO = 'magenta'
#cOguri = 'gray'
#cUmetsu = 'brown'
#cCypriano = 'gray'
#cGruen = 'purple'
sAll = 60
alpha_All = 0.35
alpha_HST = 0.7
zAll = 2
zWtG = 9
zMantz = 1
zHST = 10
sMantz = 20
sHST = 40
#pl.subplot(211)
#pl.scatter(wp_x["M500"],pw_x["MSZ"],color=cAll,s=sAll,label=r'Other WL',zorder=zAll,alpha=alpha_All)#,facecolors='none')
#pl.scatter(wp_x[WtG]["M500"],pw_x[WtG]["MSZ"],color=cWtG,s=10,label=r'WtG',zorder = zWtG )
s1 = pl.errorbar(wp_x[WtG]["M500"],pw_x[WtG]["MSZ"], \
                 xerr=wp_x[WtG]["Delta_M500"],yerr=pw_x[WtG]["MSZ_ERR_UP"], \
                 color=cWtG,fmt='o',label=r'WtG',zorder = zWtG )
#pl.scatter(wp_x[JACO]["M500"],pw_x[JACO]["MSZ"],color=cJACO,s=10,label=r'Mahdavi+13')
#pl.scatter(wp_x[Oguri]["M500"],pw_x[Oguri]["MSZ"],color=cOguri,s=10,label=r'Oguri+12')
#pl.scatter(wp_x[Umetsu]["M500"],pw_x[Umetsu]["MSZ"],color=cUmetsu,s=10,label=r'Umetsu+14')
#pl.scatter(wp_x[Cypriano]["M500"],pw_x[Cypriano]["MSZ"],color=cCypriano,s=10,label=r'Cypriano+04')
#pl.scatter(wp_x[Gruen]["M500"],pw_x[Gruen]["MSZ"],color=cGruen,s=10,label=r'Gruen+14')
#pl.scatter(m10p_x["M500"],pm10_x["MSZ"],s=sMantz,color=cMantz,label=r'Mantz+10 (X-ray)',zorder=zMantz,marker='+')
#Plot a 1:1 relation
line1, = pl.plot([1,40],[1,40],label='1:1',color=cone)#,'--')
line1.set_dashes([6,2,1,2])
line2, = pl.plot([1,40],[0.57,40*0.57],color=cPlanck,label='43% underestimate')
#line.set_dashes([6,2,6,2,6,2])
#temp = np.polyfit(pw_x["MSZ"],wp_x["M500"],1)
good_x = np.squeeze(np.argwhere(pw_x["MSZ"] > 4.9))
#temp = np.polyfit(wp_x["M500"][good_x],pw_x["MSZ"][good_x],1)
#temp = np.polyfit(wp_x[np.squeeze(WtG)]["M500"],pw_x[np.squeeze(WtG)]["MSZ"],1)
#WtG log-log
temp = np.polyfit(np.log10(wp_x[np.squeeze(WtG)]["M500"]),np.log10(pw_x[np.squeeze(WtG)]["MSZ"]),1)
#The above gets [0.309,0.568]
temp = [0.68,np.log10((10/10**0.68)*0.699)]
#temp = np.polyfit(np.log10(wp_x["M500"][good_x]),np.log10(pw_x["MSZ"][good_x]),1)
nstep = 1
xvals = np.arange(0.5,40,nstep)
#yvals = temp[1]+temp[0]*xvals
yvals = 10**(temp[1]+temp[0]*np.log10(xvals))
line3, = pl.plot(xvals,yvals,cbf,label='WtG best-fit')
#line.set_dashes([6,2,6,2,6,2])
nvals = len(hst_x["MSZ"])
xhst = 10**((np.log10(hst_x["MSZ"])-temp[1])/temp[0])*(1.0+np.random.normal(scale=0.15,size=nvals))
s2 = pl.scatter(xhst,hst_x["MSZ"],label=lHST,color=cHST, \
                zorder=zHST,marker='D',edgecolor=ceHST,s=sHST,alpha=alpha_HST)
#pl.xlabel('$M_{500}$ (Planck)',fontsize=fsize)
#pl.ylabel(r'$M_{500,\rm various} \ (10^{14}M_{\odot})$',fontsize=0.7*fsize)
pl.ylabel(r'Planck SZ Mass $(M_{500};\ 10^{14}M_{\odot})$',fontsize=fsize)
pl.xlabel(r'Lensing Mass $(M_{500} ; \ 10^{14}M_{\odot})$',fontsize=fsize)
xmin = 3.0#0.35
xmax = 40
pl.xlim([xmin,xmax])
pl.ylim([4.0,21.0])
pl.gca().set_xscale('log')
#pl.gca().set_xticklabels(['3','10'])
pl.gca().set_yscale('log')
axes = pl.subplot(111)
axes.xaxis.set_minor_formatter(FormatStrFormatter("%i"))
axes.xaxis.set_major_formatter(FormatStrFormatter("%i"))
axes.yaxis.set_minor_formatter(FormatStrFormatter("%i"))
axes.yaxis.set_major_formatter(FormatStrFormatter("%i"))
#pl.subplot(311)
#pl.legend(bbox_to_anchor=(2.5,1.0),scatterpoints=1,ncol=3,labelspacing=0.5,columnspacing=0)
pl.legend([line1,line2,line3,s2,s1], \
          [lone,lCMB,lWtGbf,lHST,lWtG], \
#          bbox_to_anchor=(-0.01,1.2), \
          loc=2,frameon=False, \
          scatterpoints=1,ncol=1,labelspacing=0.75,columnspacing=0, \
          numpoints=1)
#pl.legend(loc=2,scatterpoints=1,ncol=3,labelspacing=0.5,columnspacing=0)
pl.subplots_adjust(left=0.15,right=0.95,top=0.85,bottom=0.15)
pl.savefig('mass_compare_all.jpg',format='jpg')
#pl.subplot(212)
pl.scatter(wp_x["M500"],pw_x["MSZ"]/wp_x["M500"],color=cAll,s=sAll,label=r'WL Masses',zorder=zAll,alpha=alpha_All)
pl.scatter(wp_x[WtG]["M500"],pw_x[WtG]["MSZ"]/wp_x[WtG]["M500"], \
           color=cWtG,s=10,label=r'WL Masses',zorder=zWtG)
#pl.scatter(wp_x[JACO]["M500"],pw_x[JACO]["MSZ"]/wp_x[JACO]["M500"], \
#           color=cJACO,s=10,label=r'WL Masses')
#pl.scatter(wp_x[Oguri]["M500"],pw_x[Oguri]["MSZ"]/wp_x[Oguri]["M500"], \
#           color=cOguri,s=10,label=r'WL Masses')
#pl.scatter(wp_x[Umetsu]["M500"],pw_x[Umetsu]["MSZ"]/wp_x[Umetsu]["M500"], \
#           color=cUmetsu,s=10,label=r'WL Masses')
#pl.scatter(wp_x[Cypriano]["M500"],pw_x[Cypriano]["MSZ"]/wp_x[Cypriano]["M500"], \
#           color=cCypriano,s=10,label=r'WL Masses')
#pl.scatter(wp_x[Gruen]["M500"],pw_x[Gruen]["MSZ"]/wp_x[Gruen]["M500"], \
#           color=cGruen,s=10,label=r'WL Masses')
#pl.scatter(m10p_x["M500"],pm10_x["MSZ"]/m10p_x["M500"],s=sMantz,color=cMantz,label=r'Mantz+10 (X-ray)',zorder=zMantz,marker='+')
tempx = 10**((np.log10(hst_x["MSZ"])-temp[1])/temp[0])
pl.plot(xvals,yvals/xvals,color=cbf)
pl.plot([0.1,40],[1,1],color=cone)
line, = pl.plot([0.1,40],[0.57,0.57],color=cPlanck,label='43% underestimate')
line.set_dashes([6,2,1,2])
pl.scatter(tempx,hst_x["MSZ"]/tempx,label=lHST,color=cHST,zorder=zHST,marker='D', \
           s=sHST,alpha=alpha_HST)#edgecolor=ceHST,
#
#pl.scatter(pw_x["MSZ"],pw_x["MSZ"]/wp_x["M500"],color='black',s=10,label=r'WL Masses')
#pl.scatter(pw_x[WtG]["MSZ"],pw_x[WtG]["MSZ"]/wp_x[WtG]["M500"], \
#           color=cWtG,s=10,label=r'WL Masses')
#pl.scatter(pw_x[JACO]["MSZ"],pw_x[JACO]["MSZ"]/wp_x[JACO]["M500"], \
#           color=cJACO,s=10,label=r'WL Masses')
#pl.scatter(pw_x[Oguri]["MSZ"],pw_x[Oguri]["MSZ"]/wp_x[Oguri]["M500"], \
#           color=cOguri,s=10,label=r'WL Masses')
#pl.scatter(pw_x[Umetsu]["MSZ"],pw_x[Umetsu]["MSZ"]/wp_x[Umetsu]["M500"], \
#           color=cUmetsu,s=10,label=r'WL Masses')
#pl.scatter(pw_x[Cypriano]["MSZ"],pw_x[Cypriano]["MSZ"]/wp_x[Cypriano]["M500"], \
#           color=cCypriano,s=10,label=r'WL Masses')
#pl.scatter(pw_x[Gruen]["MSZ"],pw_x[Gruen]["MSZ"]/wp_x[Gruen]["M500"], \
#           color=cGruen,s=10,label=r'WL Masses')
#pl.scatter(pm10_x["MSZ"],pm10_x["MSZ"]/m10p_x["M500"],color='green',label=r'Mantz+10 (X-ray)',marker='+')
#
pl.xlabel(r'$M_{500,\rm various} \ (10^{14}M_{\odot})$',fontsize=fsize)
#pl.ylabel(r'$\frac{M}{M}$')
pl.ylabel(r'$\frac{M_{500,\rm Planck}}{M_{500,\rm various}}$',fontsize=fsize)
pl.xlim([xmin,xmax])
pl.ylim([0.3,20.0])
pl.gca().set_xscale('log')
pl.gca().set_yscale('log')
pl.subplots_adjust(left=0.15,right=0.925,top=0.8,bottom=0.15)
pl.savefig('mass_compare_all2.jpg',format='jpg')
############################################
pl.clf()
tsize = 5
twidth = 2
mpl.rcParams['xtick.minor.size']=tsize
mpl.rcParams['xtick.minor.width']=twidth
mpl.rcParams['xtick.major.size']=tsize*3
mpl.rcParams['xtick.major.width']=twidth
mpl.rcParams['ytick.minor.size']=tsize
mpl.rcParams['ytick.minor.width']=twidth
mpl.rcParams['ytick.major.size']=tsize*2
mpl.rcParams['ytick.major.width']=twidth
fig = pl.figure(4,dpi=200)
pl.rcParams['axes.linewidth']=2.0
#pl.plot(xvals,yvals/xvals,color=cbf)
nbins=24
xmin = 1.0
xmax = 60.0
t_alpha = 0.45
tweights = 1.0*np.ones_like(p_x["MSZ"])#/len(p_x["MSZ"])
WtG_all = np.argwhere(w_x['AuthorCODE']=='applegate+14')
pl.hist(10**((np.log10(p_x["MSZ"])-temp[1])/temp[0]), \
        histtype='stepfilled',label='Planck Clusters', \
        bins=np.logspace(np.log10(xmin),np.log10(xmax),nbins/2),weights=tweights,alpha=t_alpha)
pl.hist(10**((np.log10(hst_x["MSZ"])-temp[1])/temp[0]), \
        histtype='stepfilled',label=lHST, \
        bins=np.logspace(np.log10(xmin),np.log10(xmax),nbins/2),color=cHST)
pl.hist(w_x[WtG_all]["M500"],alpha=t_alpha+0.3, \
        histtype='stepfilled', \
        label='WtG',bins=np.logspace(np.log10(xmin),np.log10(xmax),nbins/2),hatch="//",fill=False)
pl.xlabel(r'$M_{500} \ [10^{14}M_{\odot}]$',fontsize=fsize)
pl.ylabel('Number of Clusters',fontsize=fsize)
pl.title('Cluster Mass Distribution',fontsize=fsize)
pl.legend(loc='upper left')#,bbox_to_anchor=(-0.25,1.0))#,loc=3,scatterpoints=1,ncol=3,labelspacing=0.5,columnspacing=0)
pl.subplots_adjust(left=0.15,right=0.925,top=0.9,bottom=0.15)
pl.gca().set_xscale('log')
pl.ylim([0.0,17.0])
pl.savefig('mass_hist.jpg',format='jpg')
#interact(local=locals())
