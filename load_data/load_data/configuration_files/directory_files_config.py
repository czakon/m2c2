########################################################################
# PURPOSE: Create one location in which we can find where all of the
#          directories are located.
#
# Created by Nicole Gisela Czakon 11/12/2015
# USAGE
# from configuration_files import directory_files_config as dir_config
########################################################################
import os
homeDir = os.environ['HOME'] + '/'
dbPlotDirectory = homeDir + 'data/analysis/plots/cluster_database/'
srPlotDirectory = homeDir + 'data/analysis/plots/scaling_relations/'
srDataDirectory = homeDir + 'data/analysis/python_data/scaling_relations/'

