########################################################################
# PURPOSE: Create one location in which all of the cosmo parameters
# can be saved.
#
# Created by Nicole Gisela Czakon 09/07/2015
###################################################################
h0 = 0.7
Omega_M = 0.3
Omega_L = 0.7
