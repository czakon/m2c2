########################################################################
# PURPOSE: Create one location in which all of the sze file parameters 
# can be saved.
#
# Created by Nicole Gisela Czakon 02/17/2015
###################################################################
import os
homeDirectory = os.environ['HOME']
data_folder = homeDirectory + '/data/'
catalog = data_folder + 'catalogs/'
planck_union = catalog + 'PLANCK/HFI_PCCS_SZ-union_R2.08.fits'
planck_mmf3 = catalog + 'PLANCK/HFI_PCCS_SZ-MMF3_R2.08.fits'
planck_hst = catalog + 'PLANCK/HST_Planck_2015.txt'
planck_ami = catalog + 'AMI/ami_dx9.fits'
bolo_centroid = catalog + 'BOLOCAM/Bolocam_X-ray_centroids_Mantz.txt'
bolo_folder = catalog + 'BOLOCAM/'
bolocam_centroid = bolo_folder + 'bolocam_centroids.txt'
bolocam = bolo_folder + 'czakon_2015_table3.txt'
