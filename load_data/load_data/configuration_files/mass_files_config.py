########################################################################
# PURPOSE: Create one location in which all of the mass file parameters 
# can be saved.
#
# Created by Nicole Gisela Czakon 02/17/2015
########################################################################
import os
homeDirectory = os.environ['HOME']
data_folder = homeDirectory + '/data/'
catalog = data_folder + 'catalogs/' 
CLASH_catalog = catalog + 'CLASH/'
PLANCK_catalog = catalog + 'PLANCK/'
sereno2014_catalog = catalog + 'SERENO/'
okabe_catalog = catalog + 'OKABE/'
hoekstra2015_catalog = catalog + 'HOEKSTRA2015/'
RELICS_catalog = catalog + 'RELICS/'
WTG_catalog = catalog + 'WTG/'
merten2015 = CLASH_catalog + 'Merten2015.txt'
sereno = sereno2014_catalog + 'LC2-single_v2.0.dat'
hst2015 = PLANCK_catalog + 'HST_Planck_2015.txt'
okabe2010 = okabe_catalog + 'okabe2010.txt'
okabe2015 = okabe_catalog + 'okabe2015.txt'
okabe2015_2 = okabe_catalog + 'okabe2015_2.txt'
okabe2015_z = okabe_catalog + 'okabe2015_z.txt'
hoekstra2015 = hoekstra2015_catalog + 'hoekstra2015.txt'
relics = RELICS_catalog + 'RELICS_ngc.txt'
umetsu2015_radec = CLASH_catalog + 'umetsu2015_tbl1.txt'
umetsu2015_c = CLASH_catalog + 'umetsu2015_tbl2.txt'
umetsu2015_masses = CLASH_catalog + 'umetsu2015_tbl3.txt'
vonderlinden2012 = WTG_catalog + 'wtg2012_radec.txt'
applegate2014 = WTG_catalog + 'wtg2014_masses.txt'
