########################################################################
# PURPOSE: Create one location in which all of the xray file parameters 
# can be saved.
#
# Created by Nicole Gisela Czakon 05/08/2015
###################################################################
#One of the data folders is for when I run the code locally on my laptop
#and one is for when I run the code on a remote cluster.
#data_folder = r"C:/Users/czakon/Google Drive/temp_data/" 
import os
homeDirectory = os.environ['HOME']
data_folder = homeDirectory + '/data/'
catalog = data_folder+"catalogs/" 
#Here are other samples that we might want to include here.
#BONAMENTE,CLASH,COMPILATIONS,MACS,MANTZ,MAUGHAN,MCXC
donahue2015Table1 = catalog + 'CLASH/donahue_xray_clash_2015_table1.txt'
#donahue2015Table1 = catalog + 'CLASH/donahue_xray_clash_2015_table1.txt'
#Keep this and include it when necessary.
#donahue2014Table4 = 'CLASH/donahue_xray_clash_2015_table4txt'
mcxc = catalog + 'MCXC/mcxc_fits.fits'
#bolo_centroid = catalog + 'BOLOCAM/Bolocam_X-ray_centroids_Mantz.txt'
bcs = catalog+'ROSAT/BCS.table.txt'
ebcs = catalog+'ROSAT/eBCS.table.txt'
reflex = catalog+'ROSAT/REFLEX.txt'
mantz2010_data = catalog + 'MANTZ/mantz2010_data.txt'
mantz2010_radec = catalog + 'MANTZ/mantz2010_radec.txt'
vikhlinin2009_lowz = catalog + 'V09/V09_lz.txt'
vikhlinin2009_highz = catalog + 'V09/V09_hz.txt'
four00dmain = catalog + 'V09/400dmain.dat'
mahdavi2013_tbl1 = catalog + 'MAHDAVI2013/mahdavi2013_table1.tex'
mahdavi2013_tbl2 = catalog + 'MAHDAVI2013/mahdavi2013_table2.tex'
landry2013_masses = catalog + 'LANDRY2013/vikh_masses_tblA5.dat'
landry2013_z = catalog + 'LANDRY2013/bcsSample_short_no_ref_DA_tbl1.dat'

