########################################################################
# PURPOSE: Create one location in which all of the richness file
# parameters can be saved.
#
# Created by Nicole Gisela Czakon 04/25/2015
########################################################################
import os
homeDirectory = os.environ['HOME']
data_folder = homeDirectory + '/data/'
catalog = data_folder + 'catalogs/'
andreon2016 = catalog + 'ANDREON/andreon2016.txt'
redmapperCatalog = catalog + 'REDMAPPER/redmapper_dr8_public_v6.3_catalog.fits'
redmapperMembers = catalog + 'REDMAPPER/redmapper_dr8_public_v6.3_members.fits'
