# NAME: load_masses
# 
# PURPOSE:
# To make a uniform package to load all of my different mass files
#
# USAGE: from load_data import load_masses
#        wtgData = load_masses.applegate2014()
#
# MODIFICATION HISTORY:
# 02/17/2015 NGC: Created
#########################################################################
# STANDARD PYTHON PACKAGES
import os,sys,re
from code import interact
from copy import deepcopy
# EXTERNAL PACKAGES
import numpy as np
from numpy import array
from astropy.io import ascii
from astropy.table import join
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.cosmology import FlatLambdaCDM
# M2C2 Packages
from cluster_database.configuration_files.sample_cluster_json \
    import cluster_json,cluster_measurement
from cluster_database.configuration_files.sample_cluster_json \
    import basic_measurement as cMeas
from load_data.configuration_files import cosmo_config
from load_data.configuration_files import mass_files_config
from clusters.cluster_tools import cluster as clusterR
# GLOBAL Variables
global cosmo
cosmo = FlatLambdaCDM(H0=cosmo_config.h0*100,Om0=cosmo_config.Omega_M)

#########################################################################
def applegate2014():

    catalog = "applegate2014"
    filename = mass_files_config.vonderlinden2012
    wtg_radec = ascii.read(filename)
    filename = mass_files_config.applegate2014
    wtg_masses = ascii.read(filename)
    wtg_data = join(wtg_radec,wtg_masses)
    wtg_all = []
    print(str(len(wtg_data)) +
          " clusters in the Applegate et al. (2014) sample....")
    
    for row in wtg_data:

        temp_coord = SkyCoord(row["RA"] + " " + row["DEC"],
                              unit=(u.hourangle,u.deg))

        cj = deepcopy(cluster_measurement)
        cj["cluster_id"] = row["cluster_id"]
        cj["redshift"]["bf"] = float(row["z"])
        mpc_per_arcmin = \
            cosmo.kpc_proper_per_arcmin(cj["redshift"]["bf"]).value

        cj["ra"] = temp_coord.ra.deg # Degrees
        cj["decl"] = temp_coord.dec.deg # Degrees
        
        cj["radius"] = {"scale":deepcopy(cMeas),"1p5Mpc":deepcopy(cMeas)}

        # Convert Mpc to arcmin
        cj["radius"]["scale"]["bf"] = float(row["rs"]) / mpc_per_arcmin
        cj["radius"]["scale"]["sig"]["upper"] =  \
            float(row["rs_upper"]) / mpc_per_arcmin
        cj["radius"]["scale"]["sig"]["lower"] = \
            float(np.abs(row["rs_lower"])) / mpc_per_arcmin
        cj["radius"]["scale"]["sig"]["median"] =  \
            float(0.5*(row["rs_upper"] + np.abs(row["rs_lower"]))) \
            / mpc_per_arcmin

        cj["radius"]["scale"]["bf"] = float(row["rs"]) / mpc_per_arcmin
        cj["radius"]["scale"]["sig"]["upper"] =  \
            float(row["rs_upper"]) / mpc_per_arcmin
        cj["radius"]["scale"]["sig"]["lower"] = \
            float(np.abs(row["rs_lower"])) / mpc_per_arcmin
        cj["radius"]["scale"]["sig"]["median"] =  \
            float(0.5*(row["rs_upper"] + np.abs(row["rs_lower"]))) \
            / mpc_per_arcmin
        
        #M(<1.5Mpc)#convert 10^14Msol to 10^14Msol/h 
        cj["mass"] = {"1p5Mpc":deepcopy(cMeas)}        
        cj["mass"]["1p5Mpc"]["bf"] = float(row["M1p5Mpc"]*cosmo.h)
        
        cj["mass"]["1p5Mpc"]["sig"]["upper"] = \
            float(row["M1p5Mpc_upper"]*cosmo.h)
        cj["mass"]["1p5Mpc"]["sig"]["lower"] = \
            float(np.abs(row["M1p5Mpc_lower"]*cosmo.h))
        cj["mass"]["1p5Mpc"]["sig"]["median"] = \
            float(0.5*(row["M1p5Mpc_upper"] \
                       + np.abs(row["M1p5Mpc_lower"]))*cosmo.h)
        
        cj["c"] = {"200":deepcopy(cMeas)}
        cj["c"]["200"]["bf"] = 4.0

        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj

        wtg_all.append(cluster_j)

    return wtg_all

#########################################################################
def umetsu2015():

    catalog = "umetsu2015"
    filename= mass_files_config.umetsu2015_masses
    umetsu2015_masses = ascii.read(filename)
    filename= mass_files_config.umetsu2015_c
    umetsu2015_c = ascii.read(filename)
    filename= mass_files_config.umetsu2015_radec
    umetsu2015_radec = ascii.read(filename)
    umetsu2015_data = join(umetsu2015_radec,umetsu2015_masses)
    umetsu2015_data = join(umetsu2015_data,umetsu2015_c)
    umetsu2015_all = []
    
    print(str(len(umetsu2015_data)) +
          " clusters in the Umetsu et al. (2015) sample....")
    
    for row in umetsu2015_data:
        
        temp_coord = SkyCoord(row["RA"]+row["DEC"],
                              unit=(u.hourangle,u.deg))

        cj = deepcopy(cluster_measurement)        

        cj["cluster_id"] = row["cluster_id"]
        cj["redshift"]["bf"] = float(row["z"])
        cj["ra"] = temp_coord.ra.deg
        cj["decl"] = temp_coord.dec.deg

        cj["c"] = {"200":deepcopy(cMeas)}
        cj["c"]["200"]["bf"] = float(row["c200"])
        cj["c"]["200"]["median"] = float(row["c200_err"])
        cj["c"]["200"]["upper"] = float(row["c200_err"])
        cj["c"]["200"]["lower"] = float(row["c200_err"])

        cj["radius"] = {"scale":deepcopy(cMeas)}
        cj["radius"]["scale"]["bf"] = float(row["rs"])
        cj["radius"]["scale"]["sig"]["median"] = float(row["rs_err"])
        cj["radius"]["scale"]["sig"]["upper"] = float(row["rs_err"])
        cj["radius"]["scale"]["sig"]["lower"] = float(row["rs_err"])
               
        all_radii = ["2500","1000","500","200","100","vir","200m"]
        cj["mass"] = {}
        cj["radius"] = {}
        
        for rX in all_radii:
            
            cj["mass"][rX] = deepcopy(cMeas)
            
            #convert 10^14Msol to 10^14Msol/h
            massString = "M" + rX
            errString = massString + "_err"
            cj["mass"][rX]["bf"] = float(row[massString]*cosmo.h) 
            cj["mass"][rX]["sig"]["median"] = \
                float(row[errString]*cosmo.h)
            cj["mass"][rX]["sig"]["upper"] = \
                float(row[errString]*cosmo.h)
            cj["mass"][rX]["sig"]["lower"] = \
                float(row[errString]*cosmo.h)

            # Convert this to a cluster instance, which is defined in
            # cluster_tools.
            clusterT = clusterR(
                cj["redshift"]["bf"],
                mDelta = cj["mass"][rX]["bf"],
                mDeltaErr = cj["mass"][rX]["sig"]["median"],
                delta = rX)
        
            cj["radius"][rX] = deepcopy(cMeas)
            cj["radius"][rX]["bf"] = clusterT.rDeltaArcmin
            cj["radius"][rX]["sig"]["median"] = \
                clusterT.rDeltaArcminErr
            cj["radius"][rX]["sig"]["upper"] = \
                clusterT.rDeltaArcminErr
            cj["radius"][rX]["sig"]["lower"] = \
                clusterT.rDeltaArcminErr
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj

        umetsu2015_all.append(cluster_j)
        
    return umetsu2015_all
'''
        # Erase the code below once we know that it's giving consistent values.
        cj["mass"] = {"500":deepcopy(cMeas)}
        cj["mass"]["500"]["bf"] = float(row["M500"]*cosmo.h)
        cj["mass"]["500"]["sig"]["median"] =   float(row["M500_err"]*cosmo.h)
        cj["mass"]["500"]["sig"]["upper"] =   float(row["M500_err"]*cosmo.h)
        cj["mass"]["500"]["sig"]["lower"] =   float(row["M500_err"]*cosmo.h)

        cj["mass"] = {"200":deepcopy(cMeas)}
        cj["mass"]["200"]["bf"] = float(row["M200"]*cosmo.h)
        cj["mass"]["200"]["sig"]["median"] =   float(row["M200_err"]*cosmo.h)
        cj["mass"]["200"]["sig"]["upper"] =   float(row["M200_err"]*cosmo.h)
        cj["mass"]["200"]["sig"]["lower"] =   float(row["M200_err"]*cosmo.h)

        cj["mass"] = {"100":deepcopy(cMeas)}
        cj["mass"]["100"]["bf"] = float(row["M100"]*cosmo.h)
        cj["mass"]["100"]["sig"]["median"] =   float(row["M100_err"]*cosmo.h)
        cj["mass"]["100"]["sig"]["upper"] =   float(row["M100_err"]*cosmo.h)
        cj["mass"]["100"]["sig"]["lower"] =   float(row["M100_err"]*cosmo.h)

        cj["mass"] = {"vir":deepcopy(cMeas)}
        cj["mass"]["vir"]["bf"] = float(row["Mvir"]*cosmo.h)
        cj["mass"]["vir"]["sig"]["median"] =  float(row["Mvir_err"]*cosmo.h)
        cj["mass"]["vir"]["sig"]["upper"] =  float(row["Mvir_err"]*cosmo.h)
        cj["mass"]["vir"]["sig"]["lower"] =  float(row["Mvir_err"]*cosmo.h)

        cj["mass"] = {"200m":deepcopy(cMeas)}
        cj["mass"]["200m"]["bf"] = row["M200m"]*cosmo.h                  
        cj["mass"]["200m"]["sig"]["median"] =  row["M200m_err"]*cosmo.h
        cj["mass"]["200m"]["sig"]["upper"] =  row["M200m_err"]*cosmo.h
        cj["mass"]["200m"]["sig"]["lower"] =  row["M200m_err"]*cosmo.h
'''
#########################################################################
def merten2015():

    catalog = "merten2015"
    filename = mass_files_config.merten2015
    merten_data = ascii.read(filename)
    merten_all = []
    print(str(len(merten_data)) +
          " clusters in the Merten et al. (2015) sample....")
    
    for row in merten_data:

        cj = deepcopy(cluster_measurement)        
        cj["cluster_id"] = row["cluster_id"]
        cj["ra"] = row["RA"]
        cj["decl"] = row["DEC"]
        cj["redshift"]["bf"] = row["redshift"]

        all_radii = ["2500","500","200","vir"]
        
        #cj["mass"] = {"2500":deepcopy(cMeas), \
        #                           "500":deepcopy(cMeas), \
        #                           "200":deepcopy(cMeas), \
        #                           "vir":deepcopy(cMeas)}

        cj["mass"] = {}
        cj["c"] = {}
        cj["radius"] = {}
        
        for rX in all_radii:
            
            cj["mass"][rX] = deepcopy(cMeas)
            
            massString = "M" + rX
            errString = massString + "_err"
        
            #convert from 1E15/h SOLAR_MASS to 1E14/h SOLAR_MASS
            cj["mass"][rX]["bf"] = 10.*row[massString]
            cj["mass"][rX]["sig"]["upper"] = 10.*row[errString]
            cj["mass"][rX]["sig"]["lower"] = 10.*row[errString]
            cj["mass"][rX]["sig"]["median"] = 10.*row[errString]
            
            cj["c"][rX] = deepcopy(cMeas)
            cString = "c" + rX
            errString = cString + "_err"

            cj["c"][rX]["bf"] = 10.*row[cString]
            cj["c"][rX]["sig"]["upper"] = 10.*row[errString]
            cj["c"][rX]["sig"]["lower"] = 10.*row[errString]
            cj["c"][rX]["sig"]["median"] = 10.*row[errString]

            # Convert this to a cluster instance, which is defined in
            # cluster_tools.
            clusterT = clusterR(
                cj["redshift"]["bf"],
                mDelta = cj["mass"][rX]["bf"],
                mDeltaErr = cj["mass"][rX]["sig"]["median"],
                delta = rX)
        
            cj["radius"][rX] = deepcopy(cMeas)
            cj["radius"][rX]["bf"] = clusterT.rDeltaArcmin
            cj["radius"][rX]["sig"]["median"] = \
                clusterT.rDeltaArcminErr
                                                     
            cj["radius"][rX]["sig"]["upper"] = \
                clusterT.rDeltaArcminErr
            cj["radius"][rX]["sig"]["lower"] = \
                clusterT.rDeltaArcminErr
            
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj

        merten_all.append(cluster_j)

    return merten_all
            
# Delete this once the code is working.
#        cj["mass"]["500"]["bf"] = 10.*row["M500"]
#        cj["mass"]["500"]["sig"]["upper"] = 10.*row["M500_err"]
#        cj["mass"]["500"]["sig"]["lower"] = 10.*row["M500_err"]
#        cj["mass"]["500"]["sig"]["median"] = 10.*row["M500_err"]
#        cj["mass"]["200"]["bf"] = 10.*row["M200"]
#        cj["mass"]["200"]["sig"]["upper"] = 10.*row["M200_err"]#
#        cj["mass"]["200"]["sig"]["lower"] = 10.*row["M200_err"]
#        cj["mass"]["200"]["sig"]["median"] = 10.*row["M200_err"]
#        cj["mass"]["vir"]["bf"] = 10.*row["Mvir"]
#        cj["mass"]["vir"]["sig"]["upper"] = 10.*row["Mvir_err"]
#        cj["mass"]["vir"]["sig"]["lower"] = 10.*row["Mvir_err"]
#        cj["mass"]["vir"]["sig"]["median"] = 10.*row["Mvir_err"]
#
#        cj["c"] = {"2500":deepcopy(cMeas), \
#                                   "500":deepcopy(cMeas), \
#                                   "200":deepcopy(cMeas), \
#                                   "vir":deepcopy(cMeas)}
#        cj["c"]["500"]["bf"] = 10.*row["c500"]
#        cj["c"]["500"]["sig"]["upper"] = 10.*row["c500_err"]
#        cj["c"]["500"]["sig"]["lower"] = 10.*row["c500_err"]
#        cj["c"]["500"]["sig"]["median"] = 10.*row["c500_err"]
#        cj["c"]["200"]["bf"] = 10.*row["c200"]
#        cj["c"]["200"]["sig"]["upper"] = 10.*row["c200_err"]
#        cj["c"]["200"]["sig"]["lower"] = 10.*row["c200_err"]
#        cj["c"]["200"]["sig"]["median"] = 10.*row["c200_err"]
#        cj["c"]["vir"]["bf"] = 10.*row["cvir"]
#        cj["c"]["vir"]["sig"]["upper"] = 10.*row["cvir_err"]
#        cj["c"]["vir"]["sig"]["lower"] = 10.*row["cvir_err"]
#        cj["c"]["vir"]["sig"]["median"] = 10.*row["cvir_err"]
#########################################################################
def sereno():

    catalog = "sereno"
    filename = mass_files_config.sereno
    sereno_data = ascii.read(filename,guess=False)
    sereno_all = []
    
    print(str(len(sereno_data)) + " clusters in the Sereno sample....")
    
    for row in sereno_data:

        cj = deepcopy(cluster_measurement)        
        cj["cluster_id"] =str(row["cluster_id"])
        cj["redshift"]["bf"] = float(row["z"])
	temp_coord = SkyCoord(row["RA"] + " " + row["DEC"],
                              unit=(u.hourangle,u.deg))
        cj["ra"] = temp_coord.ra.deg
        cj["decl"] = temp_coord.dec.deg

        cj["mass"] = {}
        cj["radius"] = {}
    
        all_radii = ["2500","500","200","vir"]

        for rX in all_radii:

            cj["mass"][rX] = deepcopy(cMeas)
            
            massString = "M" + rX
            errString = massString + "_err"
            
            cj["mass"][rX]["bf"] = float(row[massString]*cosmo.h)
            cj["mass"][rX]["sig"]["median"] = \
                float(row[errString]*cosmo.h)
            cj["mass"][rX]["sig"]["upper"] = \
                float(row[errString]*cosmo.h)
            cj["mass"][rX]["sig"]["lower"] = \
                float(row[errString]*cosmo.h)
            
            # Convert this to a cluster instance, which is defined in
            # cluster_tools.
            clusterT = clusterR(
                cj["redshift"]["bf"],
                mDelta = cj["mass"][rX]["bf"],
                mDeltaErr = cj["mass"][rX]["sig"]["median"],
                delta = rX)
        
            cj["radius"][rX] = deepcopy(cMeas)
            cj["radius"][rX]["bf"] = clusterT.rDeltaArcmin
            cj["radius"][rX]["sig"]["median"] = clusterT.rDeltaArcminErr
            cj["radius"][rX]["sig"]["upper"] = clusterT.rDeltaArcminErr
            cj["radius"][rX]["sig"]["lower"] = clusterT.rDeltaArcminErr
            
	cluster_j = deepcopy(cluster_json)
        #cluster_j[row["AuthorCODE"]] = cj
        cluster_j[catalog] = deepcopy(cj)

        sereno_all.append(cluster_j)

    return sereno_all

#Delete the code below once you get the code to work.
#cj["mass"]["500"]["bf"] = float(row["M500"]*cosmo.h)
#cj["mass"]["200"]["bf"] = float(row["M200"]*cosmo.h)
#cj["mass"]["vir"]["bf"] = float(row["Mvir"]*cosmo.h)
#        cj["mass"]["500"]["sig"]["median"] = \
#            float(row["M500_err"]*cosmo.h)
#        cj["mass"]["200"]["sig"]["median"] = \
#            float(row["M200_err"]*cosmo.h)
#        cj["mass"]["vir"]["sig"]["median"] = \
#            float(row["Mvir_err"]*cosmo.h)
#        cj["mass"]["500"]["sig"]["uppper"] = \
#            float(row["M500_err"]*cosmo.h)
#        cj["mass"]["200"]["sig"]["upper"] = float(row["M200_err"]*cosmo.h)
#        cj["mass"]["vir"]["sig"]["upper"] = float(row["Mvir_err"]*cosmo.h)
#        cj["mass"]["2500"]["sig"]["lower"] = \
#            float(row["M2500_err"]*cosmo.h)
#        cj["mass"]["500"]["sig"]["lower"] = float(row["M500_err"]*cosmo.h)
#        cj["mass"]["200"]["sig"]["lower"] = float(row["M200_err"]*cosmo.h)
#        cj["mass"]["vir"]["sig"]["lower"] = float(row["Mvir_err"]*cosmo.h)
#########################################################################

def relics():

    catalog = "relics"
    filename = mass_files_config.relics
    relics = open(filename)
    relics_all = []
    relics_line = relics.readline()
    relics_line = relics_line.strip()

    while relics_line:

        if relics_line[0]!=";":
            
            cluster_j = deepcopy(cluster_json)
            cj = deepcopy(cluster_measurement)            
            column = re.split(r"\t+",relics_line)
            cj["cluster_id"] = column[1]
            cj["mass"] = {}
            cj["mass"]["500"] = {}
            cj["mass"]["500"]["bf"] = float(column[4])
            #float(m_x[j]["MASS_500"]*1E-14*cosmo.h)
            #convert to 1E14/h SOLAR_MASS
            cj["redshift"] = {}
            cj["redshift"]["bf"] = float(column[5])
            cj["ra"] = float(column[6])
            cj["decl"] = float(column[7])
            cj["Planck_SNR"] = float(column[8])
            cluster_j = deepcopy(cluster_json)
            cluster_j[catalog] = cj

            relics_all.append(cluster_j)
            
        relics_line = relics.readline()
        relics_line = relics_line.strip()

    print(str(len(relics_all)) + " clusters in the RELICS sample....")
    relics.close()
    return relics_all
#########################################################################

def okabe2010():

    catalog = "okabe2010"
    filename = mass_files_config.okabe2010
    okabe2010_data = ascii.read(filename,guess=False)
    okabe2010_all = []
    print(str(len(okabe2010_data)) +
          " clusters in the Okabe et al. (2010) sample....")
    
    for row in okabe2010_data:

	cluster_j = deepcopy(cluster_json)
        cj = deepcopy(cluster_measurement)        
        temp_coord = SkyCoord(row["RA"]+row["DEC"],unit=(u.hourangle,u.deg))
        cj["cluster_id"] = row["cluster_id"] 
        cj["redshift"]["bf"] = float(row["z"])
        cj["ra"] = temp_coord.ra.deg
        cj["decl"] = temp_coord.dec.deg

        # X-ray measured at 0.1-2.4keV(obtained from ROSAT)
        # Units: 10^44erg*s^-1
        cj["lx"] = {"500":deepcopy(cMeas)}
        cj["lx"]["500"]["bf"] = float(row["lx"])

        all_radii = ["2500","500","200","vir"]
        cj["mass"] = {}
        
        for rX in all_radii:

            massString = "M"+rX
            errString = massString + "_err_"
            cj["mass"][rX]["bf"] = float(row[massString])
            cj["mass"][rX]["sig"]["upper"] = \
                float(row[errString + "upper"])
            cj["mass"][rX]["sig"]["lower"] = \
                float(np.abs(row[errString + "lower"]))
            cj["mass"][rX]["sig"]["median"] = \
                float(0.5*(row[errString+"upper"] \
                           + np.abs(row[errString+"lower"])))
            
            # Convert this to a cluster instance, which is defined in
            # cluster_tools.
            clusterT = clusterR(
                cj["redshift"]["bf"],
                mDelta = cj["mass"][rX]["bf"],
                mDeltaErr = cj["mass"][rX]["sig"]["median"],
                delta = rX)
        
            cj["radius"][rX] = deepcopy(cMeas)
            cj["radius"][rX]["bf"] = clusterT.rDeltaArcmin
            cj["radius"][rX]["sig"]["median"] = clusterT.rDeltaArcminErr
            cj["radius"][rX]["sig"]["upper"] = clusterT.rDeltaArcminErr
            cj["radius"][rX]["sig"]["lower"] = clusterT.rDeltaArcminErr
            
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj
        okabe2010_all.append(cluster_j)
    
    return okabe2010_all
''' Erase this once the code is working
        cj["mass"] = {"2500":deepcopy(cMeas), \
                                   "500":deepcopy(cMeas), \
                                   "200":deepcopy(cMeas), \
                                   "vir":deepcopy(cMeas)}
        cj["mass"]["200"]["bf"] = float(row["M200"])
        cj["mass"]["200"]["sig"]["upper"] = float(row["M200_err_upper"])
        cj["mass"]["200"]["sig"]["lower"] = float(np.abs(row["M200_err_lower"]))
        cj["mass"]["200"]["sig"]["median"] = \
            float(0.5*(row["M200_err_upper"] + np.abs(row["M200_err_lower"])))
        cj["mass"]["500"]["bf"] = float(row["M500"])
        cj["mass"]["500"]["sig"]["upper"] = float(row["M500_err_upper"])
        cj["mass"]["500"]["sig"]["lower"] = float(np.abs(row["M500_err_lower"]))
        cj["mass"]["500"]["sig"]["median"] = \
            float(0.5*(row["M500_err_upper"]+np.abs(row["M500_err_lower"])))
        cj["mass"]["2500"]["bf"] = float(row["M2500"])
        cj["mass"]["2500"]["sig"]["upper"] = float(row["M2500_err_upper"])
        cj["mass"]["2500"]["sig"]["lower"] = \
            float(np.abs(row["M2500_err_lower"]))
        cj["mass"]["2500"]["sig"]["median"] = \
            float(0.5*(row["M2500_err_upper"] + np.abs(row["M2500_err_lower"])))
'''
#########################################################################

def okabe2015():

    #This is the most recent LOCUSS catalog. 
    catalog = "okabe2015"
    filename = mass_files_config.okabe2015
    okabe2015_1 = ascii.read(filename)
    filename = mass_files_config.okabe2015_2
    okabe2015_2 = ascii.read(filename)
    # I have some information about N and S clusters and I would like
    # to preserve it in the database
    locuss_data = join(okabe2015_1,okabe2015_2,keys="cluster_id")
    okabe2015_all = []
    
    print(str(len(locuss_data)) +
          " clusters in the Okabe et al. (2015) sample....")
    
    for row in locuss_data:

        temp_coord = SkyCoord(row["RA"]+row["DEC"],unit=(u.hourangle,u.deg))
        cluster_j = deepcopy(cluster_json)
        cj = deepcopy(cluster_measurement)        
        cj["cluster_id"] = row["cluster_id"] 
        cj["redshift"]["bf"] = float(row["z"])
        cj["ra"] = temp_coord.ra.deg
        cj["decl"] = temp_coord.dec.deg

        cj["c"] = {"200":deepcopy(cMeas)}
        cj["c"]["200"]["bf"] = 3.69
        cj["c"]["200"]["sig"]["upper"] = 0.26
        cj["c"]["200"]["sig"]["lower"] = -0.24

        all_radii = ["2500","1000","500","200","vir"]
        cj["mass"] = {}
        cj["radius"] = {}
        
        for rX in all_radii:
            
            massString = "m" + rX
            errString = massString + "_err_"
            cj["mass"][rX] = deepcopy(cMeas)
            #10^14 Msol*h-1
            cj["mass"][rX]["bf"] = float(row[massString]) 
            cj["mass"][rX]["sig"]["upper"] = \
                float(row[errString+"upper"])
            cj["mass"][rX]["sig"]["lower"] = \
                float(np.abs(row[errString+"lower"]))
            cj["mass"][rX]["sig"]["median"] = \
                float(0.5*(row[errString+"upper"] \
                           + np.abs(row[errString+"lower"])))
            
            # Convert this to a cluster instance, which is defined in
            # cluster_tools.
            clusterT = clusterR(
                cj["redshift"]["bf"],
                mDelta = cj["mass"][rX]["bf"],
                mDeltaErr = cj["mass"][rX]["sig"]["median"],
                delta = rX)
        
            cj["radius"][rX] = deepcopy(cMeas)
            cj["radius"][rX]["bf"] = clusterT.rDeltaArcmin
            cj["radius"][rX]["sig"]["median"] = clusterT.rDeltaArcminErr
            cj["radius"][rX]["sig"]["upper"] = clusterT.rDeltaArcminErr
            cj["radius"][rX]["sig"]["lower"] = clusterT.rDeltaArcminErr
            
        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj
        okabe2015_all.append(cluster_j)
        
    return okabe2015_all
''' Remove this once the code is debugged
            {"2500":deepcopy(cMeas), \
                                      "1000":deepcopy(cMeas), \
                                      "500":deepcopy(cMeas), \
                                      "200":deepcopy(cMeas), \
                                      "vir":deepcopy(cMeas)}
        cj["mass"]["200"]["bf"] = float(row["m200"])
        cj["mass"]["200"]["sig"]["upper"] = float(row["m200_err_upper"])
        cj["mass"]["200"]["sig"]["lower"] = float(np.abs(row["m200_err_lower"]))
        cj["mass"]["200"]["sig"]["median"] = \
            float(0.5*(row["m200_err_upper"] + np.abs(row["m200_err_lower"])))
        cj["mass"]["500"]["bf"] = float(row["m500"])
        cj["mass"]["500"]["sig"]["upper"] = float(row["m500_err_upper"])
        cj["mass"]["500"]["sig"]["lower"] = float(np.abs(row["m500_err_lower"]))
        cj["mass"]["500"]["sig"]["median"] = \
            float(0.5*(row["m500_err_upper"] + np.abs(row["m500_err_lower"])))
        cj["mass"]["1000"]["bf"] = float(row["m1000"])
        cj["mass"]["1000"]["sig"]["upper"] = float(row["m1000_err_upper"])
        cj["mass"]["1000"]["sig"]["lower"] = \
            float(np.abs(row["m1000_err_lower"]))
        cj["mass"]["1000"]["sig"]["median"] = \
            float(0.5*(row["m1000_err_upper"] + np.abs(row["m1000_err_lower"])))
        cj["mass"]["2500"]["bf"] = float(row["m2500"])
        cj["mass"]["2500"]["sig"]["upper"] = float(row["m2500_err_upper"])
        cj["mass"]["2500"]["sig"]["lower"] = \
            float(np.abs(row["m2500_err_lower"]))
        cj["mass"]["2500"]["sig"]["lower"] = \
            float(0.5*(row["m2500_err_upper"] + np.abs(row["m2500_err_lower"])))
'''
#########################################################################

def hoekstra2015():

    #This is the CCCP catalog
    catalog = "hoekstra2015"
    filename = mass_files_config.hoekstra2015
    hoekstra_data = ascii.read(filename)
    hoekstra_all = []
    print(str(len(hoekstra_data)) \
          + " clusters in the Hoekstra et al. (2015) sample....")
    
    for row in hoekstra_data:

        temp_coord = SkyCoord(row["RA"]+row["DEC"],
                              unit=(u.hourangle,u.deg))
	cluster_j = deepcopy(cluster_json)
        cj = deepcopy(cluster_measurement)        
        cj["cluster_id"] = row["cluster_id"] 
        cj["redshift"]["bf"] = float(row["z"])
        cj["ra"] = temp_coord.ra.deg
        cj["decl"] = temp_coord.dec.deg

        all_radii = ["2500","500","vir"]
        cj["mass"] = {}
        cj["radius"] = {}
        
        for rX in all_radii:
            
            massString = "MNFW"+rX
            errString = massString+"_err_"
            
            cj["mass"][rX] = deepcopy(cMeas)
            cj["mass"][rX]["bf"] = float(row[massString]*cosmo.h)
            cj["mass"][rX]["sig"]["upper"] = \
                float(row[errString+"upper"]*cosmo.h)
            cj["mass"][rX]["sig"]["lower"] = \
                float(np.abs(row[errString+"lower"])*cosmo.h)
            cj["mass"][rX]["sig"]["median"] = \
            float(0.5 * cosmo.h \
                  * (row[errString+"upper"] \
                     + np.abs(row[errString+"lower"])))
            # Convert this to a cluster instance, which is defined in
            # cluster_tools.
            clusterT = clusterR(
                cj["redshift"]["bf"],
                mDelta = cj["mass"][rX]["bf"],
                mDeltaErr = cj["mass"][rX]["sig"]["median"],
                delta = rX)
        
            cj["radius"][rX] = deepcopy(cMeas)
            cj["radius"][rX]["bf"] = clusterT.rDeltaArcmin
            cj["radius"][rX]["sig"]["median"] = clusterT.rDeltaArcminErr
            cj["radius"][rX]["sig"]["upper"] = clusterT.rDeltaArcminErr
            cj["radius"][rX]["sig"]["lower"] = clusterT.rDeltaArcminErr
            

        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj
        hoekstra_all.append(cluster_j)

    return hoekstra_all
''' Remove this once the code is debugged                     
        cj["mass"]["2500"]["bf"] = float(row["MNFW2500"]*cosmo.h)
        cj["mass"]["2500"]["sig"]["upper"] = \
            float(row["MNFW2500_err_upper"]*cosmo.h)
        cj["mass"]["2500"]["sig"]["lower"] = \
            float(np.abs(row["MNFW2500_err_lower"])*cosmo.h)
        cj["mass"]["2500"]["sig"]["median"] = \
            float(0.5*(row["MNFW2500_err_upper"]\
                       + np.abs(row["MNFW2500_err_lower"]))*cosmo.h)
        cj["mass"]["500"]["bf"] = float(row["MNFW500"]*cosmo.h)
        cj["mass"]["500"]["sig"]["upper"] = \
            float(row["MNFW500_err_upper"]*cosmo.h)
        cj["mass"]["500"]["sig"]["lower"] = \
            float(np.abs(row["MNFW500_err_lower"])*cosmo.h)
        cj["mass"]["500"]["sig"]["median"] = \
            float(0.5*(row["MNFW500_err_upper"] \
                       + np.abs(row["MNFW500_err_lower"]))*cosmo.h)
'''
