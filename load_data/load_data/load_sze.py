# NAME: load_sze
# 
# PURPOSE:
# To make a uniform package to load all of my different sze files
# 
# Types of files:
#
# USAGE:
#
# MODIFICATION HISTORY:
# 02/17/2015 NGC: Created
#########################################################################
# Things to do for this code.
# 1. Find what the conversion is from Y2500 to Y500
# 2. Figure out what DA is and multiply it by the relevant fraction to
#    get the right thing.
# 3. Find out what we think our apeture bias is for these measurements.
# 4. Make a plot comparing the different mass measurements.
#########################################################################
# Can load the following catalogs: AMI, planck_mmf3 (the cosmology sample)
# planck_union (the union sample), mcxc
#
# Other functionality:
# pickle_bolo_sze_overlap: since loading the SZE data and finding the
# overlap often takes a fair amount of time.
# This function finds the intersection between various catalogs and
# pickles it in a python format.
# 
# INCLUDE THE REFERENCES FOR THE CATALOGS THAT WE ARE LOADING ABOVE.
#
# STANDARD PYTHON PACKAGES
from code import interact
from copy import deepcopy
import os,pickle
# EXTERNAL PACKAGES
import numpy as np
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.cosmology import FlatLambdaCDM
from astropy.table import Table,join
from astropy.io import fits,ascii
from scipy.io import readsav
import astropy.coordinates as coord
import astropy.units as units
# M2C2 Packages
from load_data.configuration_files import cosmo_config
import load_data.configuration_files.sze_files_config as sze_files_config
from cluster_database.configuration_files.sample_cluster_json \
    import cluster_json,cluster_measurement
from cluster_database.configuration_files.sample_cluster_json \
    import basic_measurement as cMeas
from clusters.cluster_tools import cluster as clusterR
# GLOBAL Variable
global cosmo
cosmo = FlatLambdaCDM(H0=cosmo_config.h0*100,Om0=cosmo_config.Omega_M)

def ami():

    catalog = "ami"
    filename = sze_files_config.planck_ami
    planck_ami = fits.open(filename)
    ami_fits = planck_ami[1]
    ami_all = []
    #Here are the columns that we want to keep for the ami: (ami.columns)
    #for j in range(nami):
    for row in ami_fits.data:

        if row["AMI_MATCH"] > 0:

            cj = deepcopy(cluster_measurement)

            cj["cluster_id"] = row["Planck_ID"]
            #cluster_j["nominal"]["cluster_id"] = row["Planck_ID"]
            cj["redshift"]["bf"] = float(row["Planck_z"])
            cj["ra"] = float(row["AMI_RA"])#degree
            cj["decl"] = float(row["AMI_Dec"])#degree
            
            cj["radius"] = {"500":deepcopy(cMeas)}
            cj["radius"]["500"]["bf"] = \
                float(row["AMI_THETA_500"]) #arcmin
            cj["radius"]["500"]["sig"]["median"] = \
                float(row["AMI_ETHETA_500"]) #arcmin
            cj["radius"]["500"]["sig"]["upper"] = \
                float(row["AMI_ETHETA_500_U"]) #arcmin
            cj["radius"]["500"]["sig"]["lower"] = \
                float(np.abs(row["AMI_ETHETA_500_L"])) #arcmin

            cj["ysz"] = {"500":deepcopy(cMeas)}
            cj["ysz"]["500"]["bf"] = \
                float(row["AMI_Y500"])#arcmin^2
            cj["ysz"]["500"]["sig"]["median"] = \
                float(row["AMI_ETHETA_500"])#arcmin^2
            cj["ysz"]["500"]["sig"]["upper"] = \
                float(row["AMI_EY500_U"])#arcmin^2
            cj["ysz"]["500"]["sig"]["lower"] = \
                float(np.abs(row["AMI_EY500_L"]))#arcmin^2

            cluster_j = deepcopy(cluster_json)
            cluster_j[catalog] = cj

            ami_all.append(cluster_j)

    print(str(len(ami_all)) + " clusters in the AMI sample....")
    
    return ami_all
#########################################################################
# This hasn"t been coded in the proper format yet.
# def planck_mmf3():
#    filename = sze_files_config.planck_mmf3
#    planck_mmf3 = fits.open(filename)
#    mmf3_fits = planck_mmf3[1]
#    return mmf3_fits
#    #Keep the Examples Below:
#    #Obtaining the redshift
#    #dr2_z = planck_mmf3[1].data["REDSHIFT"]
#    # In order to convert from Y 5R500 to Y500
#    # planck_mmf3[1].data["Y5R500"]*planck_mmf3[0].header["pp_y2yfh"]
#    return planck_mmf3
#########################################################################
def planck_union():

    catalog = "union"
    filename = sze_files_config.planck_union
    planck_union = fits.open(filename)
    union_fits = [[],[]]
    #conversion factor from Y to Y500
    union_fits[0] = planck_union[0].header["PP_Y2YFH"]  
    union_fits[1] = planck_union[1]
    #Here are the columns that we want to keep for the ami: (ami.columns)
    conversion_factor = union_fits[0]
    union_fits = union_fits[1]
    nunion = len(union_fits.data)
    union_all = []
    
    print(str(len(union_fits.data)) +
          " clusters in the Planck Union sample....")
    
    for row in union_fits.data:

        cj = deepcopy(cluster_measurement)
        
        #cluster_j = deepcopy(cluster_json)
        #cluster_j[catalog] = deepcopy(cluster_measurement)        
        cj["cluster_id"] = row["Name"]
        #cluster_j["nominal"]["cluster_id"] = row["Name"]
        cj["redshift"]["bf"] = float(row["REDSHIFT"])
        cj["ra"] = float(row["RA"])#degree
        cj["decl"] = float(row["Dec"])#degree
        
        #convert to arcmin^2
        # NOTE! Will not convert to Mpc here because not all of the values
        # have redshifts....
        cj["ysz"] = {"500":deepcopy(cMeas)}
        scaleY = 0.001*conversion_factor
        cj["ysz"]["500"]["bf"] = float(row["Y5R500"]*scaleY)
        cj["ysz"]["500"]["sig"]["median"] = \
            float(row["Y5R500_ERR"]*scaleY)
        cj["ysz"]["500"]["sig"]["upper"] = \
            float(row["Y5R500_ERR"]*scaleY)
        cj["ysz"]["500"]["sig"]["lower"] = \
            float(row["Y5R500_ERR"]*scaleY)

        cj["mass"] = {"500":deepcopy(cMeas)}
        #convert 10^14 Msol to 10^14Msol/h
        cj["mass"]["500"]["bf"] = float(row["MSZ"]*cosmo.h)
        #Need to fix these error bars, #10^14 Msol/h
        cj["mass"]["500"]["sig"]["upper"] = \
            float(row["MSZ_ERR_UP"]*cosmo.h)
        cj["mass"]["500"]["sig"]["lower"] = \
            float(np.abs(row["MSZ_ERR_LOW"])*cosmo.h)
        cj["mass"]["500"]["sig"]["median"] = \
            float((np.abs(row["MSZ_ERR_UP"]) \
                   +np.abs(row["MSZ_ERR_LOW"]))*0.5*cosmo.h)
        # Convert this to a cluster instance, which is defined in
        # cluster_tools.
        clusterT = clusterR(
            cj["redshift"]["bf"],
            mDelta=cj["mass"]["500"]["bf"],
            mDeltaErr=cj["mass"]["500"]["sig"]["median"],
            delta=500)
        
        cj["radius"] = {"500":deepcopy(cMeas)}
        cj["radius"]["500"]["bf"] = clusterT.rDeltaArcmin
        cj["radius"]["500"]["sig"]["median"] = clusterT.rDeltaArcminErr
        cj["radius"]["500"]["sig"]["upper"] = clusterT.rDeltaArcminErr
        cj["radius"]["500"]["sig"]["lower"] = clusterT.rDeltaArcminErr

        cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj

        union_all.append(cluster_j)

    return union_all
#########################################################################

def czakon2015():

    catalog = "czakon2015"
    filename = sze_files_config.bolocam_centroid
    bolo_radec = ascii.read(filename)
    filename = sze_files_config.bolocam
    bolo_ym = ascii.read(filename)
    bolo_data = join(bolo_radec,bolo_ym)
    bolo_all = []
    print(str(len(bolo_data)) + \
          " clusters in the Czakon et al. (2015) sample..")
    
    for row in bolo_data:

        temp_coord = SkyCoord(row["RA"]+""+row["DEC"],
                              unit=(u.hourangle,u.deg))

        cj = deepcopy(cluster_measurement)        
        #cluster_j[catalog]["matching_catalog"] = catalog
        #cluster_j[catalog]["matching_id"] = row["cluster_id"]
        cj["cluster_id"] = row["cluster_id"]
        #cluster_j["nominal"]["cluster_id"] = row["cluster_id"]
        cj["redshift"]["bf"] = float(row["z"])
        cj["ra"] = temp_coord.ra.deg
        cj["decl"] = temp_coord.dec.deg

        cj["radius"] = {"2500":deepcopy(cMeas)}
        cj["radius"]["2500"]["bf"] = float(row["r2500"])  #Mpc
        cj["radius"]["2500"]["sig"]["upper"] = \
            float(row["r2500_upper"])  #Mpc
        cj["radius"]["2500"]["sig"]["lower"] = \
            float(np.abs(row["r2500_lower"]))  #Mpc

        cj["mass"] = {"2500":deepcopy(cMeas)}
        #convert M^14Msol to M^14Msol/h 
        cj["mass"]["2500"]["bf"] = float(row["M2500"]*cosmo.h)
        cj["mass"]["2500"]["sig"]["upper"] = \
            float(row["M2500_upper"]*cosmo.h)
        cj["mass"]["2500"]["sig"]["lower"] = \
            float(np.abs(row["M2500_lower"])*cosmo.h)
        cj["mass"]["2500"]["sig"]["median"] = \
            float((np.abs(row["M2500_upper"])\
                   + np.abs(row["M2500_lower"]))*0.5*cosmo.h)
        cj["tx"] = {"500":deepcopy(cMeas)}
        cj["tx"]["500"]["bf"] = float(row["kT"]) #keV
        #keV
        cj["tx"]["500"]["sig"]["median"] = float(row["kT_err"])
        #keV
        cj["tx"]["500"]["sig"]["upper"] = float(row["kT_err"])
        #keV
        cj["tx"]["500"]["sig"]["lower"] = float(row["kT_err"]) 

        #NGC 09082015 Need to check this conversion
        cj["ysz"] = {"2500":deepcopy(cMeas)}
        #convert 1E-10 ster to arcmin^2
        conversion_factor = (1E-10)*(180*60/np.pi)**2
        cj["ysz"]["2500"]["bf"] = \
            float(row["Y2500"]*conversion_factor)
        cj["ysz"]["2500"]["sig"]["upper"] = \
            float(row["Y2500_upper"]*conversion_factor)
        cj["ysz"]["2500"]["sig"]["lower"] = \
            float(np.abs(row["Y2500_lower"])*conversion_factor)
        cj["ysz"]["2500"]["sig"]["median"] = \
            float((row["Y2500_upper"] \
                   + np.abs(row["Y2500_lower"]))*0.5*conversion_factor)
        
	cluster_j = deepcopy(cluster_json)
        cluster_j[catalog] = cj
        bolo_all.append(cluster_j)

    return bolo_all
    

