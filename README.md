# README #

Welcome to the Multiwavelength Meta Cluster Catalog (M2C2). This software is still in beta. If you're interested in contributing to this project, please send the owner of the repository a message through bitbucket. 

This README will help you get set up and running the database. This repository also includes an [overview paper](https://bitbucket.org/czakon/m2c2/src/796005ba14a43da60c3efabeef4f027e19a6b325/documentation/m2c2_overview.pdf?at=master), which gives more information about the database itself.

### Quick Summary ###

The Multiwavelength Meta Cluster Catalog (M2C2) (beta) is a galaxy cluster database including the latest galaxy cluster measurements. 

### How do I get set up? ###

Please check the wiki to see how to get setup. 

### Who do I talk to? ###

* If you have any questions on installation, or how to contribute to this project, please contact the owner of this repository using the Bitbucket messaging service.