# Usage: this code only needs to be run one.
# python setup.py develop --user
# 07/04/2015 Created by Nicole Czakon
# packages=['load_data','configuration_files'], \
from setuptools import setup
from setuptools import find_packages

setup(name='cluster_database', \
      description = 'this package contains all of the code that I need to create the cluster database.', \
      author='Nicole Czakon', \
      packages=find_packages(), \
      zip_sage=False)


