# Keep this as a reference on how to fill in the database.
# Created by Nicole Czakon 07/20/2015
# Example:
# from cluster_database.configuration_files.sample_cluster_json import cluster_json
from copy import deepcopy
basic_measurement = {"bf":0.0,"sig":{"median":0.0,"upper":0.0,"lower":0.0}}
radial_measurement = {"units":"insert unit here"}
                      #"2500":deepcopy(basic_measurement), \
                      #"500":deepcopy(basic_measurement), \
                      #"200":deepcopy(basic_measurement), \
                      #"vir":deepcopy(basic_measurement), \
                      #"full":deepcopy(basic_measurement), \
                      #"scale":deepcopy(basic_measurement),\
                      
#Default Units
#RA/Dec Degrees (J2000)
#Mass: 10^14 Msun/h
#Radius: arcmin
#Ysz: arcmin^2
#Lx: erg/s
#Tx: keV
cluster_measurement = {"cluster_id":"insert name here","alt_cluster_id":"",
                       "redshift":deepcopy(basic_measurement), \
                       "ra":0.0,"decl":0.0,\
                       "multiple_system":"N",\
                       "matching_id":"insert matching id"}#, \
#                       "matching_catalog":"insert here"}
#                       "mass":deepcopy(radial_measurement), \
#                       "mgas":deepcopy(radial_measurement), \
#                       "radius":deepcopy(radial_measurement), \
#                       "ysz":deepcopy(radial_measurement), \
#                       "lx":deepcopy(radial_measurement), \
#                       "tx":deepcopy(radial_measurement),\
#                       "subcatalog_name":"insert subcatalog",\
#                       "found_matches":"N",\
                       #"catalog" : "insert catalog"}
# The nominal measurements will enventually be the self-consistent measurements
# that we will be using for the analysis. 
cluster_json = {"nominal":deepcopy(cluster_measurement)}#, \
#                "mcxc":deepcopy(cluster_measurement), \
#                "bcs" :deepcopy(cluster_measurement),\
#                "ebcs":deepcopy(cluster_measurement),\
#                "reflex":deepcopy(cluster_measurement),\
#                "union":deepcopy(cluster_measurement),\
#                "landry2013":deepcopy(cluster_measurement),\
#                "mahdavi2013":deepcopy(cluster_measurement),\
#                "mmf3":deepcopy(cluster_measurement), \
#                "czakon2015":deepcopy(cluster_measurement), \
#                "ami":deepcopy(cluster_measurement),\
#                "merten2015":deepcopy(cluster_measurement),\
#                "sereno":deepcopy(cluster_measurement),\
#                "hst2015":deepcopy(cluster_measurement),\
#                "mantz2010":deepcopy(cluster_measurement),\
#                "vikhlinin2009":deepcopy(cluster_measurement),\
#                "okabe2015":deepcopy(cluster_measurement),\
#                "hoekstra2015": deepcopy(cluster_measurement),\
#                "relics": deepcopy(cluster_measurement),\
#                "umetsu2015":deepcopy(cluster_measurement),\
#                "okabe2010": deepcopy(cluster_measurement),\
#                "applegate2014": deepcopy(cluster_measurement)}
