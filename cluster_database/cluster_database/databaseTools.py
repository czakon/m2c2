#!/usr/bin/env python
#The purpose of this module is to allow you to easily access the information from the database
#without having to remember all of the pymongo commands each time.
# MODIFICATION HISTORY
# 04/15/2016: NGC Created
# from cluster_database.databaseTools import findClosestCluster
# temp = findClosestCluster(22.9708,-13.6094)
import difflib
from pymongo import MongoClient
import numpy as np
from astropy import units as u
from astropy.coordinates import SkyCoord
from astroquery.ned import Ned
import cluster_database.configuration_files.databaseConfiguration as database_config
from code import interact

def findClosestCluster(RA,Dec,clusterId=False):
    # Initialize the port to communicate with the database
    client = MongoClient(database_config.server,database_config.port)
    db = client.clusters#Can call db.collection_names
    clusters = db.clusters
    ###
    #Do a coordinate search with Ned
    co = SkyCoord(ra=RA,dec=Dec,unit=(u.deg,u.deg),frame='icrs')
    result = Ned.query_region(co,radius=2.0*u.arcmin,equinox='J2000.0')
    goodX = np.where(result['Type']=='GClstr')
    if len(goodX[0])==1:
        result = result[goodX[0][0]]
        nominalId = result['Object Name']
    elif len(goodX[0])==0:
        print('No suitable cluster found in the NED query, will use the name given in the catalog.')
        nominalId = None
    else:
        result = result[goodX]
        if clusterId:
            if 'macs' in clusterId:
                clusterId = clusterId.replace('macs','MACS J')
                
            print('Multiple clusters returned, will find the one with the best name match....')
            try:
                nominalId = difflib.get_close_matches(clusterId,result['Object Name'].tolist())[0]
            except:
                print('\nNo good name matches, will choose the closest cluster instead\n')
                print(result['Object Name'].tolist())
                interact(local=locals())

        else:
            goodY = np.argmin(result['Distance (arcmin)'])
            result = result[goodY]
            nominalId = result['Object Name']
        print('[catalogId,nedId]')
        print([clusterId,nominalId])

    #searchRadius = 10.0
    #print('\nSearching for a matching cluster within a ' + str(searchRadius) + ' arcminute radius.')
    #searchRadius /=60.
    #RA = float(RA)
    #Dec = float(Dec)
    #cluster = clusters.find({"$and":[{"nominal.ra":{"$gt":RA-searchRadius}}, \
    #                       {"nominal.ra":{"$lt":RA+searchRadius}}, \
    #                       {"nominal.decl":{"$gt":Dec-searchRadius}}, \
    #                       {"nominal.decl":{"$lt":Dec+searchRadius}}]})
    cluster = clusters.find({"nominal.cluster_id":{"$eq":nominalId}})

    if cluster.count()==1:
        print('Success, only one cluster found in this region.\n')
    else:
        print(str(cluster.count()) + ' clusters found, modify the databaseTools program until it does what you want it to do.')
        interact(local=locals())
    return cluster[0]

if __name__ == '__main__':

    temp = findClosestCluster(22.9708,-13.6094)
    interact(local=locals())
