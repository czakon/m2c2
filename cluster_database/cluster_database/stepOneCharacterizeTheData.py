#!/usr/bin/env python2.7
# TO DO: Need to rerun the database wit new czakon2015
#########################################################################
# MODIFICATION HISTORY: 
# 04/08/2015 NGC Created (much of this is copied from
#                         characterizeClusterDatabase)
#
# USAGE:
# ./stepOneCharacterizeTheData.py
#########################################################################
# Standard Python Packages
import sys, argparse, pickle, time
from os import path
from rpy2.robjects.packages import importr
from rpy2.robjects import FloatVector
from datetime import date
from copy import copy
from code import interact
#########################################################################
# Supplemental Python Packages
from astropy.cosmology.core import FlatLambdaCDM
from astropy.io import fits
import yaml
import numpy as np
from numpy import random as npr
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.mlab as mlab
from pymongo import MongoClient
#########################################################################
# M2C2 Packages
from clusters import cluster_tools
from fit_data import linmixerr_tools as lmx
from load_data.configuration_files import cosmo_config
from load_data.configuration_files import directory_files_config \
    as dir_config
from cluster_database.configuration_files import databaseConfiguration \
    as dbConfig
#########################################################################
# Global Variables
global args
global cosmo
cosmo = FlatLambdaCDM(H0=cosmo_config.h0*100,Om0=cosmo_config.Omega_M)

#########################################################################
# Functions
# mpcToArcmin(radiusMpc,redshift)
# verifyConfigurationParameters(clusters,xyConfig)
# getSampleData(clusterSample,xyConfig,saxis,nsample)
# findDataRatio(xData,yData)
# characterizeError(sData,pdf_plot=None)
# fitFgas()
# fitSample(xData,yData,nxSample,nySample,xyConfig,fitRatio=True)
# plotSample(xyConfig,xData,yData,plotUncut=None,sfits=None,
#            pdf_plot=None):
#########################################################################
def mpcToArcmin(radiusMpc,redshift):

    return radiusMpc/(cosmo.kpc_proper_per_arcmin(redshift).value*1E-3)

#########################################################################
def verifyConfigurationParameters(clusters,xyConfig):

    if "aperture_reference" not in xyConfig.keys():
        xyConfig["aperture_reference"] = ""
    
    if len(xyConfig["xaxis"]["samples"]) is not 1:

        print("\nIn it's current confguration, this program can only"
              " plot one sample on the x-axis at time.\n")
        interact(local=locals())

    else:

        xsample = xyConfig["xaxis"]["samples"][0]["name"]
        ysamples = [xyConfig["yaxis"]["samples"][i]["name"] \
                    for i in range(len(xyConfig["yaxis"]["samples"]))]
        
    print("Warning: Will default to only using the overdensity "
          "specified in the y-axis for now.")
    
    xproperty = xyConfig["xaxis"]["samples"][0]["property"]
    yproperties = [xyConfig["yaxis"]["samples"][i]["property"]
                   for i in range(len(ysamples))]

    if xproperty != "redshift":
        xapt = str(xyConfig["xaxis"]["samples"][0]["overdensity"])
        xquery = xsample + "." + xproperty + "."+ xapt + ".bf"
    else:
        xapt = ""
        xquery = xsample + "." + xproperty + ".bf"

    yapts = []
    yqueries = []

    for indx,yproperty in enumerate(yproperties):

        if yproperty != "redshift":

            yapt = str(xyConfig["yaxis"]["samples"][indx]
                            ["overdensity"])
            yapts.append(yapt)
            yqueries.append(ysamples[indx] + "." + yproperty + "."
                            + yapt + ".bf")
                            

        else:

            yapts.append("")
            yqueries.append(ysamples[indx] + "." + yproperty + ".bf")

    allQueries = [{yqueries[i]:{"$gt":0.0}} for i in range(len(yqueries))]
    allQueries.append({xquery:{"$gt":0.0}})

    cutQueries = copy(allQueries)

    print("\nallQueries:\n")
    for query in allQueries:
        print(query)
        
    allSample = clusters.find({"$and":allQueries})
    
    print("\n" + str(allSample.count()) + \
          " clusters before applying any cuts...\n")

    if "cuts" in xyConfig.keys():

        for x,item in enumerate(xyConfig["cuts"]):
            
            xSample = item.keys()[0]
            xProperty = item[xSample]["property"]
            xMin = item[xSample]["min"]
            xMax = item[xSample]["max"]
            
            # This is for any property that does not have an apt,
            # could probably make this more general in the future.
            if xProperty != "redshift":
                xApt = str(item[xSample]["overdensity"])
                xQuery = xSample + "." + xProperty + "."+ xApt + \
                         ".bf"
            else:
                xQuery = xSample + "." + xProperty + ".bf"

            cutQueries.append({xQuery:{"$gt":xMin}})

            if xMax > xMin:
                cutQueries.append({xQuery:{"$lt":xMax}})

        cutSample = clusters.find({"$and":cutQueries})
        print(cutQueries)
        print("\n" + str(cutSample.count()) + \
              " clusters after applying all cuts...\n")
    else:
        cutSample = allSample

    if cutSample.count() == 0:

        print("\nWARNING: the parameters that you entered are not "
              "valid...please check the entries in the database and "
              "try again.\n")

        interact(local=locals())

    if "cuts" not in xyConfig.keys():
        run_id = ysamples[0] + "_" + yproperties[0] + yapts[0] + \
                 "_" + xsample + "_" + xproperty + xapt
        xyConfig["cut_id"] = "No Cuts"
    else:
        csample = xyConfig["cuts"][0].keys()[0]
        cproperty = xyConfig["cuts"][0][csample]["property"]
        cmin = str(xyConfig["cuts"][0][csample]["min"])
        xyConfig["cut_id"] = "cut_" + csample + "_" + \
                                  cproperty + "_" + cmin
        run_id = ysamples[0] + "_" + yproperties[0] + yapts[0] + \
                 "_" + xsample + "_" + xproperty + xapt + "_" + \
                 xyConfig["cut_id"]
        
    xyConfig["run_id"] = run_id
    
    filePrepend = dir_config.dbPlotDirectory + \
                  "databaseCharacterization_" + \
                  date.today().isoformat() + "_" + run_id
                  
    xyConfig["plot_filename"] = filePrepend + ".pdf"

    #If the two axes are the same, plot the ratio as well
    if xyConfig["xaxis"]["samples"][0]["property"] \
       == xyConfig["yaxis"]["samples"][0]["property"]:
        
        xyConfig["plotRatio"] = True
        xyConfig["rplot_filename"] = filePrepend + "_ratio.pdf"
        
    elif (xyConfig["xaxis"]["samples"][0]["property"] == 'mass') \
         and (xyConfig["yaxis"]["samples"][0]["property"] == 'mgas'):
        
        xyConfig["plotRatio"] = True
        xyConfig["rplot_filename"] = filePrepend + "_fgas.pdf"
        
    else:
        xyConfig["plotRatio"]=False

    return allSample, cutSample, xyConfig

#########################################################################
def getSampleData(clusterSample,xyConfig,saxis,nsample):

    sValues = []
    sErrors = []
    sradii = []
    new_sValues = []
    sredshifts = []

    if xyConfig[saxis]["samples"][nsample]["property"] == "redshift":
        sapt = ""
    else:
        sapt = \
            str(xyConfig[saxis]["samples"][nsample]["overdensity"])
        
    ssample = xyConfig[saxis]["samples"][nsample]["name"]
    sProp = xyConfig[saxis]["samples"][nsample]["property"]
    sscale = xyConfig[saxis]["samples"][nsample]["fitScale"]
    if sscale == 0:
        print("Error, the sscale value is a multiplicative value and"
              " can not be set to zero.")
    #Include the factor of E(z) in the axis labels
    if "hPower" in xyConfig[saxis]["samples"][nsample].keys():

        if ("h_0" not in
            xyConfig[saxis]["samples"][nsample]["axisLabel"]) and \
            xyConfig[saxis]["samples"][nsample]["hPower"]!=0.0:

            if xyConfig[saxis]["samples"][nsample]["hPower"]==1.0:
            
                xyConfig[saxis]["samples"][nsample]["axisLabel"] = \
                        "$h_0$" + \
                        xyConfig[saxis]["samples"][nsample]["axisLabel"]
                
            else:

                xyConfig[saxis]["samples"][nsample]["axisLabel"] = \
                    "$h_0^{" + \
                    str(xyConfig[saxis]["samples"][nsample]
                        ["hPower"]) + "}$" + \
                        xyConfig[saxis]["samples"][nsample]["axisLabel"]
    else:
        xyConfig[saxis]["samples"][nsample]["hPower"] = 0.0
    ###
    if "ezPower" in xyConfig[saxis]["samples"][nsample].keys():

        if ("E(z)" not in
            xyConfig[saxis]["samples"][nsample]["axisLabel"]) and \
            xyConfig[saxis]["samples"][nsample]["ezPower"]!=0.0:

            if xyConfig[saxis]["samples"][nsample]["ezPower"]==1.0:
            
                xyConfig[saxis]["samples"][nsample]["axisLabel"] = \
                        "$E(z)$" + \
                        xyConfig[saxis]["samples"][nsample]["axisLabel"]
                
            else:

                xyConfig[saxis]["samples"][nsample]["axisLabel"] = \
                    "$E(z)^{" + \
                    str(xyConfig[saxis]["samples"][nsample]
                        ["ezPower"]) + "}$" + \
                        xyConfig[saxis]["samples"][nsample]["axisLabel"]
    else:
        xyConfig[saxis]["samples"][nsample]["ezPower"] = 0.0

    #This is needed because the mass is defined wrt h^(-1)Mpc
    if sProp == "mass" or sProp=="mgas":
        hFactor = -1
    else:
        hFactor = 0
    for cluster in clusterSample:

        #redshifts.append(cluster["nominal"]["redshift"]["bf"])
        sredshift = cluster[ssample]["redshift"]["bf"]
        # Mass values are in units of 1E14/h
        # Radial measurements are in units of Mpc.
        # Remember, that the concentration must be given with respect
        # to the apt.
        
        if sProp == "redshift":
            svalue = cluster[ssample][sProp]["bf"]
            # This is just temporary code until we incorporate the error
            # correctly.
            svalue_error = svalue*0.0 
        else:
            #radius = mpcToArcmin(cluster[xsample]["radius"][xapt]["bf"],
            #                     cluster[xsample]["redshift"])
            conversion_factor = \
                cosmo.efunc(sredshift) \
                ** (-xyConfig[saxis]["samples"][nsample]["ezPower"]) \
                * (cosmo.H0.value/100.0) \
                ** (hFactor- xyConfig[saxis]["samples"][nsample]["hPower"])
            svalue = cluster[ssample][sProp][sapt]["bf"] * conversion_factor
            svalue_error = cluster[ssample][sProp][sapt]["sig"]["median"] \
                           * conversion_factor

        if sProp == "ysz":

            conversion_factor = cosmo.kpc_proper_per_arcmin(
                cluster[ssample]["redshift"]["bf"]).value**2.0*1E-6
            svalue = svalue * conversion_factor
            svalue_error = svalue_error * conversion_factor
            #sradius = mpcToArcmin(
            #   cluster[xsample]["radius"][xapt]["bf"],
            #   cluster[xsample]["redshift"])
            
        yac = xyConfig["aperture_reference"]
        
        if yac and (yac != ssample):

            print("\nRecalculating " + ssample + "\'s mass at " + \
                  yac + "\'s apt...")
            print("Stop here, make sure that it can evaluate the aperture"
                  " correction appropriately.")
            interact(local=locals())
            old_value = {
                "mass":svalue,"apt":int(sapt),
                "apt_radius": cluster[ssample]["radius"][sapt]["bf"], 
                "scale_radius": cluster[ssample]["radius"]["scale"]["bf"], \
                "concentration": {
                    "bf": cluster[ssample]["c"][c_delta[ssample]]["bf"],
                    "delta":c_delta[ssample]}} 

            if not old_value["apt_radius"]:
                print("In the current version of this program,"
                      " the aperture radius needs to be set.")
                interact(local=locals())

            new_value = {
                "mass":0.0,"apt":0, \
                "apt_radius": cluster[yac]["radius"][sapt]["bf"], \
                "scale_radius": cluster[yac]["radius"]["scale"]["bf"], \
                "concentration": {"bf": cluster[yac]["c"][c_delta[yac]]["bf"], \
                                  "delta":c_delta[yac]}} 

            new_value,old_value = cluster_tools.apt_correction(
                old_value,new_value, cluster[ssample]["redshift"])

            new_values.append(new_value["mass"])
            
        sValues.append(svalue)
        sErrors.append(svalue_error)
        sredshifts.append(sredshift)            

    #Convert to numpy objects
    sValues = np.array(sValues)
    sErrors = np.array(sErrors)
    sredshifts = np.array(sredshifts)
    goodSv = sValues > sErrors

    # We will renormalize the masses by 10, see what the does to the
    # uncertainty.
    sLog = np.log10(sValues) +  np.log10(sscale)
    sLogerr = 0.5*(np.log10(sValues + sErrors) - \
                   np.log10(sValues - sErrors))
    
    measured_data = {"values":sValues,"errors":sErrors,
                     "sLog":sLog,"sLogerr":sLogerr,
                     "redshifts":sredshifts, "goodSv":goodSv}
    
    return measured_data

#########################################################################
def findDataRatio(xData,yData):

    xValues = xData["values"]
    yValues = yData["values"]
    xErrors = xData["errors"]
    yErrors = yData["errors"]
    rValues = yValues/xValues
    
    # Propagation of Errors
    try:
        rErrors = np.abs(rValues) * \
                  np.sqrt((xErrors/xValues)**2 + (yErrors/yValues)**2)
        rLog = np.log10(rValues)
        rLogErr = 0.5*((np.log10(rValues+rErrors)) \
                       - (np.log10(rValues-rErrors)))
        xlogerr = 0.5*((np.log10(xValues+xErrors)) \
                       - (np.log10(xValues-xErrors)))
    except:
        
        print('It looks like there is a problem in finding the erorrs.')
        interact(local=locals())
    
    return {"values":rValues,"errors":rErrors,
            "sLog":rLog,"sLogerr":rLogErr,"xycovar":-xlogerr**2}
            #"redshifts":rRedshifts,"goodSv":goodSv}

#########################################################################
def characterizeError(sData,pdf_plot=None):

    relError = sData["errors"]/sData["values"]
    sDataHist = np.histogram(sData["sLog"])
    zDataHist = np.histogram(sData["redshifts"])
    #Just move this up so that the binning is accurate
    sDataHist[1][-1] += 1.0
    binError = []
    binX = []
    zBinError = []
    zBinX = []
            
    for j in range(len(sDataHist[1])-1):

        yDataBinned = (sData["sLog"] >= sDataHist[1][j]) & \
                      (sData["sLog"] < sDataHist[1][j+1])
        binX.append(np.mean(sData["values"][yDataBinned]))
        binError.append(np.mean(relError[yDataBinned]))

    sData["redshifts"] = np.array(sData["redshifts"])

    for j in range(len(zDataHist[1])-1):

        zDataBinned = (sData["redshifts"] >= zDataHist[1][j]) & \
                      (sData["redshifts"] < zDataHist[1][j+1])
        zBinX.append(np.mean(sData["redshifts"][zDataBinned]))
        zBinError.append(np.mean(relError[zDataBinned]))

    zBinX = np.array(zBinX)
    plt.scatter(zBinX,zBinError)
    plt.plot(zBinX,0.20*zBinX + 0.35,label="$0.20z+0.35$")
    plt.xlabel("Redshift")
    plt.ylabel("Relative Error")
    plt.legend()
    
    if pdf_plot is None:
        plt.show()
    else:
        pdf_plot.savefig()
    plt.clf()
    plt.scatter(binX,binError)
    binX.append(30)
    plt.plot(binX,0.55*np.array(binX)**(-0.225),label="$0.55M^{-0.225}$")
    plt.gca().set_xscale("log")
    plt.gca().set_yscale("log")
    plt.xlabel("Mass 1E14")
    plt.ylabel("Fractional Error")
    plt.ylim([2E-1,3.0])
    plt.legend()
    if pdf_plot is None:
        plt.show()
    else:
        pdf_plot.savefig()

    return

#########################################################################
'''
def fitFgas():
        [post_fvx,sr_uncorr] = lmx.run_linmixerr_iterate(
            xlog,flog,xlogerr,flogerr,
            xycov=-xlogerr**2,run_id=run_id)

        # This is the same as above, except that the ylogs have been
        # exchanged with xlogs and now there is covariance.
        measured_data = {"xlog":xlog,"ylog":flog,
                         "xlogerr":xlogerr,"ylogerr":flogerr,
                         "xycovar":-xlogerr**2}

        [sr_corr, sr_mock_in,all_fits_fvx] = lmx.find_linmixerr_bias(
            measured_data,scr_uncorr=sr_uncorr,run_id=run_id)

        pickle.dump([sr_corr, sr_uncorr,sr_fix,sr_mock_in,
                     post_fvx,all_fits_fvx],open(fvx_picklename,"wb"))
                            
        sr_fvx = {"corr":sr_corr,"uncorr":sr_uncorr,"fix":sr_fix,
                  "mock_in":sr_mock_in}

        junk = lmx.plot_linmix_iterations(
            all_fits_fvx,sr_fvx["corr"], sr_fvx["uncorr"],
            sr_fvx["mock_in"],pdf_plot=pdf_plot)

        #Summarize all of the fits into a structure
        post_fvx["alpha"] += sr_corr["alpha"]["bf"] \
                             - sr_uncorr["alpha"]["bf"]
        post_fvx["beta"] += sr_corr["beta"]["bf"] \
                            - sr_uncorr["beta"]["bf"]
        post_fvx["sigma"] += sr_corr["sigma"]["bf"] \
                             - sr_uncorr["sigma"]["bf"]
                
        # Need to renormalize the fgas values as well...
        post_fvx["alpha"] +=  sr_corr["beta"]*np.log10(xscale) \
                              - np.log(yscale)
        sr_corr["alpha"] += sr_corr["beta"]*np.log10(xscale) \
                            - np.log(yscale)
        sr_uncorr["alpha"] += sr_uncorr["beta"]*np.log10(xscale) \
                              - np.log(yscale)
        sr_fix["alpha"] += sr_fix["beta"]*np.log10(xscale) \
                           - np.log(yscale)
        sr_mock_in["alpha"] += sr_mock_in["beta"]*np.log10(xscale) \
                               - np.log(yscale)

    sr_fvx = {"corr":sr_corr,"uncorr":sr_uncorr,"fix":sr_fix,"mock_in":sr_mock_in}

    # Change the post to include the corrections to the best-fit
    # values.
    fvalues = fvalues * xscale
    fgas_errors = np.array(fgas_errors) * xscale
    flog += np.log10(xscale)
    #END OF THE FGAS STUFF, this code still needs to be tested.
    #for_testing_purposes = fgas_wl_covariance_mc(xlog,ylog,xlogerr,
    #                                                       ylogerr)

    return
'''
#########################################################################
def fitSample(xData,yData,nxSample,nySample,xyConfig,fitRatio=None):

    #fitRatio: still need to incorporate how to fix the slope for this.
    xapt = str(xyConfig["xaxis"]["samples"][nxSample]["overdensity"])
    xsample = xyConfig["xaxis"]["samples"][nxSample]["name"]
    xproperty = xyConfig["xaxis"]["samples"][nxSample]["property"]
    xscale = xyConfig["xaxis"]["samples"][nxSample]["fitScale"]

    yapt = str(xyConfig["yaxis"]["samples"][nySample]["overdensity"])

    if fitRatio is None:
        
        yproperty = xyConfig["yaxis"]["samples"][nxSample]["property"]
        ysample = xyConfig["yaxis"]["samples"][nySample]["name"]
        yscale = xyConfig["yaxis"]["samples"][nxSample]["fitScale"]

    else:

        #This is just a placeholder for when we get the fits up and running,
        #Probably want to improve the naming convention eventually.
        
        if xapt != yapt:
            print('The two axes apertures must currently be the same'
                  ' in order to fit the ratio of the two.')
            interact(local=locals())
            
        yproperty = xyConfig["yaxis"]["samples"][nxSample]["property"] \
                    + "_over_" + xyConfig["xaxis"]["samples"][nxSample]["property"]
        
        ysample = xyConfig["yaxis"]["samples"][nySample]["name"] + "_and_" + \
                  xyConfig["xaxis"]["samples"][nySample]["name"]
        
        #fgas is close enough to one that it doesn't need to be rescaled.
        yscale = 1.0

    yvx_picklename = dir_config.srDataDirectory + ysample +  \
                     yproperty + "_" + xsample + xproperty + ".p"
    
    if path.isfile(yvx_picklename) and not args.overwrite:

        print("\nPickle file exists, will load from memory...")
        pickle_out = pickle.load(open(yvx_picklename,"rb"))

        if args.linmixerr:
            
            sr_corr = pickle_out[0]
            sr_uncorr = pickle_out[1]
            sr_fix = pickle_out[2]
            sr_mock_in = pickle_out[3]
            post_xvy = pickle_out[4]
            all_fits_xvy = pickle_out[5]
            
        else:
            
            sr_corr = pickle_out[0]
            post_xvy = pickle_out[1]
 

    else:

        print("\nSaving fit results to: ")
        print(yvx_picklename)
        print("\nPickle file for the mg-mwl scaling relations either"
              " does not exist or you want to overwrite it, will rerun"
              " linmixerr.")
        
        beta_fix_xvy = 1.0

        xlog = xData["sLog"][xData["goodSv"] & yData["goodSv"]]
        ylog = yData["sLog"][xData["goodSv"] & yData["goodSv"]]
        xlogerr = xData["sLogerr"][xData["goodSv"] & yData["goodSv"]]
        ylogerr = yData["sLogerr"][xData["goodSv"] & yData["goodSv"]]


        if args.linmixerr:
            
            sr_fix = lmx.run_linfitex_idl(
                xlog,ylog,xlogerr,ylogerr,beta_fix_xvy,
                run_id=xyConfig["run_id"])

            [post_xvy,sr_uncorr] = lmx.run_linmixerr_iterate(
                xlog,ylog,xlogerr,ylogerr,run_id=xyConfig["run_id"])

            [sr_corr,sr_mock_in,all_fits_xvy] = \
                lmx.find_linmixerr_bias(
                    data,scr_uncorr=sr_uncorr,run_id=xyConfig["run_id"])

            pickle.dump([sr_corr,sr_uncorr,sr_fix,sr_mock_in,post_xvy,
                         all_fits_xvy],open(yvx_picklename,"wb"))

        else:

            mylira = importr("mylira")
            coda = importr("coda")
            print("\nLet's get mylira to do this!")
            x = FloatVector(xlog)
            x.threshold = FloatVector(xlog-10*xlogerr)
            y = FloatVector(ylog)
            y.threshold = FloatVector(ylog-10*ylogerr)
            delta = FloatVector([])
            delta.x = FloatVector(xlogerr)
            delta.x.threshold = FloatVector(xlogerr)
            delta.y = FloatVector(ylogerr)
            delta.y.threshold =  FloatVector(ylogerr)
            covariance = FloatVector([])
            print('Include the covariance here...')
            interact(local=locals())
            covariance.xy = FloatVector(0.0*xlog)
            z = FloatVector(xData["redshifts"][xData["goodSv"] &
                                               yData["goodSv"]])
            
            #Just putting this in didn\"t work, I don"t know why.
            #beta = FloatVector([])
            #beta.YIZ = 1.0

            myLiraOut = mylira.mylira(x,y,
                                 delta.x,delta.y,
                                 covariance.xy,
                                 y.threshold,delta.y.threshold,
                                 x.threshold,delta.x.threshold,
                                 z,export=True)
            
            summary = coda.summary_mcmc(myLiraOut.rx2(2).rx2(1))

            # print(summary(myLiraOut[[2]][[1]]))
            # #1. Empirical mean and standard deviation for each variable,
            # Keep this code! This is only way that I know how to extract
            # the information.
            x = 0 
            post_xvy = {}
            sr_corr = {}
            shapex = np.shape(myLiraOut[1][0])
            len = shapex[0]
            sr_corr["alpha"] = {}
            sr_corr["beta"] = {}
            sr_corr["gamma"] = {}
            sr_corr["alpha"]["sig"] = {}
            sr_corr["beta"]["sig"] = {}
            sr_corr["gamma"]["sig"] = {}
            sr_corr["alpha"]["bf"] = summary[0][0]
            fixSlope = False
            post_xvy["alpha"] = np.array([float(myLiraOut[1][0].rx(i)[0])
                                          for i in range(1,len+1)])
            if fixSlope:
                
                sr_corr["beta"]["bf"] = 1.0#summary[0][1]
                sr_corr["gamma"]["bf"] = summary[0][5]#6]
                sr_corr["alpha"]["sig"]["median"] = summary[0][7]#8]
                sr_corr["beta"]["sig"]["median"] = 0.0#summary[0][9]
                sr_corr["gamma"]["sig"]["median"] = summary[0][12]#4]
                post_xvy["beta"] = np.array(
                    [1.0 for i in range(x+1,x+len+1)])
                
            else:
                
                sr_corr["beta"]["bf"] = summary[0][1]
                sr_corr["gamma"]["bf"] = summary[0][6]
                sr_corr["alpha"]["sig"]["median"] = summary[0][8]
                sr_corr["beta"]["sig"]["median"] = summary[0][9]
                sr_corr["gamma"]["sig"]["median"] = summary[0][14]
                x += len
                post_xvy["beta"] = np.array([float(myLiraOut[1][0].rx(i)[0])
                                             for i in range(x+1,x+len+1)])
            x += len
            post_xvy["sigma"] = np.array([float(myLiraOut[1][0].rx(i)[0])
                                          for i in range(x+1,x+len+1)])
            pickle.dump([sr_corr,post_xvy],
                        open(yvx_picklename,"wb"))
                        

    if args.linmixerr:
        
        #Summarize all of the fits into a structure
        sr_xvy = {"corr":sr_corr,"uncorr":sr_uncorr, "fix":sr_fix,
                  "mock_in":sr_mock_in}

        # First, correct the posteriors
        post_xvy["alpha"] += sr_corr["alpha"]["bf"] \
                             - sr_uncorr["alpha"]["bf"]
        post_xvy["beta"] += sr_corr["beta"]["bf"] \
                            - sr_uncorr["beta"]["bf"]
        post_xvy["sigma"] += sr_corr["sigma"]["bf"] \
                             - sr_uncorr["sigma"]["bf"]

        # Then, shift the fits back to the original values of
        # the xproperty and yproperty.
        post_xvy["alpha"] +=  sr_corr["beta"]*np.log10(xscale) \
                              - np.log(yscale)
        sr_corr["alpha"] += sr_corr["beta"]*np.log10(xscale) \
                            - np.log(yscale)
        sr_uncorr["alpha"] += sr_uncorr["beta"]*np.log10(xscale) \
                              - np.log(yscale)
                              
        sr_fix["alpha"] += sr_fix["beta"]*np.log10(xscale) \
                           - np.log(yscale)
        sr_mock_in["alpha"] += sr_mock_in["beta"]*np.log10(xscale) \
                               - np.log(yscale)

        sr_xvy = {"corr":sr_corr,"uncorr":sr_uncorr, "fix":sr_fix, \
                  "mock_in":sr_mock_in}
    else:
        sr_xvy = {"corr":sr_corr}

    # KEEP THIS, FIGURE OUT HOW TO PLOT THE LINMIXERR ITERATIONS.
    # junk = lmx.plot_linmix_iterations(
    #    all_fits_xvy,sr_xvy["corr"], sr_xvy["uncorr"], sr_xvy["mock_in"],
    #    pdf_plot=pdf_plot)

    sfits = {"sr":sr_xvy,"post":post_xvy}
    
    return sfits

#########################################################################
def plotSample(xyConfig,xData,yData,plotUncut=None,sfits=None,pdf_plot=None):

    # plotUncut: is a sample of the uncut data that you can plot together
    #            with the cut data as well. Currently, this hasn't been
    #            tested for incorporation.
    
    plt.clf()
    plt.rcParams["font.size"] = 30
    plt.figure(figsize=(12,12))

    # The two lines below have a different format now because we allow
    # multiple samples in the y-axis, but only one sample for the x-axis.
    xValues = xData["values"][xData["goodSv"]]
    yValues = [yD["values"][xData["goodSv"]] for yD in yData]

    #This is a placeholder for until we change the code to allow multiple
    #x-axes.
    xi = 0
    
    if "axisLimScale" in xyConfig["xaxis"]["samples"][xi].keys():
        
        xLimScale = xyConfig["xaxis"]["samples"][xi]["axisLimScale"]
        xLim = [min(xValues)/xLimScale,max(xValues)*xLimScale]

    elif ("axisMin" in xyConfig["xaxis"]["samples"][xi].keys()) \
         and ("axisMax" in xyConfig["xaxis"]["samples"][xi].keys()):

        xLim = [xyConfig["xaxis"]["samples"][xi]["axisMin"],
                xyConfig["xaxis"]["samples"][xi]["axisMax"]]
    else:
        
        xLimScale = 1.5
        xLim = [min(xValues)/xLimScale,max(xValues)*xLimScale]

    xlabel =  xyConfig["xaxis"]["samples"][xi]["axisLabel"]

    #Cycle through each plot with a different y-axis.
    for yi in range(len(yData)):

        if "axisLimScale" in xyConfig["yaxis"]["samples"][yi].keys():
            yLimScale = xyConfig["yaxis"]["samples"][yi]["axisLimScale"]
            ylim = [min(yValues[yi][yValues[yi]>0])/yLimScale,
                    max(yValues[yi])*yLimScale]
        else:
            ylim = [xyConfig["yaxis"]["samples"][yi]["axisMin"],
                    xyConfig["yaxis"]["samples"][yi]["axisMax"]]

        ylabel =  xyConfig["yaxis"]["samples"][yi]["axisLabel"]
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.title(xyConfig["cut_id"] + ": " + str(len(xValues)) \
                  + " clusters",fontsize=35)
        plt.xlim(xLim)
        plt.ylim(ylim)
        plt.gca().set_xscale(xyConfig["xaxis"]["samples"][xi]["axis"])
        plt.gca().set_yscale(xyConfig["yaxis"]["samples"][yi]["axis"])

        # Plot the posteriors, and the error bars.
        # First, plot the contours for mgas
        #print('Figure this out.')
        #interact(local=locals())

        if sfits is not None:

            sr_corr = sfits[yi]["sr"]["corr"]

            if args.linmixerr is True:
                sr_uncorr = sfits[yi]["sr"]["uncorr"]
                sr_fix = sfits[yi]["sr"]["fix"]
            
            [contour_x,contour_y] = lmx.get_linmixerr_contours(
                np.log10(xLim),sfits[yi]["post"])
                
            plt.fill_between(10**contour_x,10**contour_y["2sig"]["upper"],
                             10**contour_y["2sig"]["lower"],
                             color="r",alpha=0.5)

            plt.fill_between(10**contour_x,10**contour_y["1sig"]["upper"],
                             10**contour_y["1sig"]["lower"], \
                             color="b",alpha=0.5)

            plt.plot(
                xLim,
                10**(sr_corr["beta"]["bf"]*np.log10(xLim) \
                     + sr_corr["alpha"]["bf"]), \
                label = r"best-fit slope $=" \
                +"{:.2f}".format(sr_corr["beta"]["bf"]) \
                + "\pm" + \
                "{:.2f}".format(sr_corr["beta"]["sig"]["median"]) \
                + "$",color="b")
            
            if args.linmixerr is True:
                plt.plot(xLim,
                         10**(sr_uncorr["beta"]["bf"]*np.log10(xLim) \
                              + sr_uncorr["alpha"]["bf"]), \
                         label=r"best-fit slope (uncorr)$="+\
                         "{:.2f}".format(sr_uncorr["beta"]["bf"]) \
                         + "\pm" + \
                         "{:.2f}".format(sr_uncorr["beta"]["sig"]["median"]) \
                         + "$",linestyle="--",color="k")

                plt.plot(xLim,10**(sr_fix["beta"]["bf"]\
                                   *np.log10(xLim) \
                                   + sr_fix["alpha"]["bf"]), \
                         label=r"best-fit fixed slope $="+\
                         "{:.2f}".format(sr_fix["beta"]["bf"].tolist()) \
                         + "$",linestyle=":")

        else:
            print("There are no error bars on this value, so, we won't"
                  " plot the error contours.")
                  

        #Not exactly sure why this is relevant to the legend
        if False:
            if xsample[-1]== xsample:
                plt.legend(bbox_to_anchor=(0.45,0.95),borderaxespad=0)

        if False:
        #Keep this stuff below
        #if yac:
            #if xsample != "sereno2014" and xsample != "mahdavi2013":
            new_xValues = np.array([i[str(xapt)]for i in new_xValues])
            plt.scatter(new_xValues,yvalues,label=xsample, \
                        edgecolors=colors[xsample],marker=markers[xsample],
                        facecolors="none")
            #plt.plot([xscale*min(r_masses),yscale*max(r_masses)],[1.0,1.0],
            #         "g:",label="1:1")
            #plt.plot([xscale*min(xValues),yscale*max(xValues)], \
                #     [fgas*xscale*min(xValues),fgas*yscale*max(xValues)],"b--")

        if False:
            plt.legend(loc=2)

        plt.axis("on")

        yac = False

        if yac:
            
            new_xValues = np.array([i[str(xapt)]for i in new_xValues])

            plt.scatter(new_xValues,yvalues/new_xValues,
                        edgecolors=colors[xsample],marker=markers[xsample], \
                        facecolors="none")
        goodx = np.where(yValues[yi] > 0.0)

        plt.errorbar(xValues[goodx],yValues[yi][goodx], \
                     xerr=xData["errors"][goodx],\
                     yerr=yData[yi]["errors"][goodx], \
                     fmt="o",color="g",zorder=1)

        if plotUncut is not None:

            xValues = plotUncut["xvalues"]
            yValues = plotUncut["yvalues"]
            goodx = np.where(yvalues > 0.0)
            
        plt.scatter(xValues[goodx],yValues[yi][goodx],marker="o",color="b",\
                    alpha=0.5,s=100,zorder=2)

    if "lineref" in xyConfig.keys():
        for lr in xyConfig["lineref"]:
            plt.plot(
                xLim, \
                10**(np.log10(xLim) \
                     *lr["slope"] \
                     + lr["intercept"]), \
                label = lr["label"],
                color=lr["color"],linestyle="--")
            
    plt.legend(fontsize='small',framealpha=0.5)        

    if pdf_plot is None:
        plt.show()
    else:
        pdf_plot.savefig() 
        plt.clf()
    #END OF if nclusters                

    return

########################## MAIN PROGRAM ################################
def main():

    global tic
    tic = time.time()
    
    print("\nInput YAML file is: " + args.inputYaml + "\n")
    stream = file(args.inputYaml,"r")
    xyConfig = yaml.load(stream)
    stream.close()

    # Initialize the port to communicate with the database
    client = MongoClient(dbConfig.server,dbConfig.port)
    db = client.clusters#Can call db.collection_names
    clusters = db.clustersNew

    fitData = True
    # Verify that the YAML file is in the correct xyConfig.
    allSample , cutSample, xyConfig = verifyConfigurationParameters(
        clusters,xyConfig)

    # Extract the data.
    xData = getSampleData(allSample,xyConfig,"xaxis",0)

    if "cuts" in xyConfig.keys():
        xCutData = getSampleData(cutSample,xyConfig,"xaxis",0)

    # Initialize the plotting parameters
    pdf_plot = PdfPages(xyConfig["plot_filename"])
    plt.clf()
    plt.rcParams["font.size"] = 30
    plt.figure(figsize=(12,12))

    doCharacterizeError = False
    if doCharacterizeError:
        junk = characterizeError(xData,pdf_plot=pdf_plot)

    yData = []
    rData = []
    sfits = []
    xi = 0

    for yi in range(len(xyConfig["yaxis"]["samples"])):

        allSample , cutSample, xyConfig = verifyConfigurationParameters(
            clusters,xyConfig)
        
        if "cuts" in xyConfig.keys():
            yiData = getSampleData(cutSample,xyConfig,"yaxis",yi)
                                   
        else:
            yiData = getSampleData(allSample,xyConfig,"yaxis",yi)
                                   

        yData.append(yiData)
        
        if xyConfig["plotRatio"]:
            riData = findDataRatio(xData,yiData)
            rData.append(riData)

        if fitData:

            if sum(yiData["sLogerr"]) != 0.0 :

                if "cuts" in xyConfig.keys():
                    
                    sfit0 = fitSample(xCutData,yiData,xi,yi,xyConfig)
                    
                    if xyConfig["plotRatio"]:
                        
                        rfit0 = fitSample(xCutData,riData,xi,yi,xyConfig,
                                          fitRatio=true)
                        
                else:
                    
                    sfit0 = fitSample(xData,yiData,xi,yi,xyConfig)
                    
                    if xyConfig["plotRatio"]:
                        
                        rfit0 = fitSample(xData,riData,xi,yi,xyConfig,
                                          fitRatio=true)
                    
                sfits.append(sfit0)

            else:

                print("\nThere are no error bars on the y-values, "
                      "will skip the fitting....\n")
                sfits.append(None)
        else:
            sfits.append(None)

    #Finally, plot the output.
    
    if "cuts" in xyConfig.keys():
        plotSample(xyConfig,xCutData,yData,plotUncut=None,sfits=sfits,
                   pdf_plot=pdf_plot)
    else:
        plotSample(xyConfig,xData,yData,plotUncut=None,sfits=sfits,
                   pdf_plot=pdf_plot)

    if xyConfig["plotRatio"]:
        
        print("Will plot the ratio of the x- and y-axis.")

        xyConfig["rplot_filename"] = xyConfig["plot_filename"]
        #This should be included in the configuration files...
        xyConfig["lineref"] = xyConfig["ratioref"]
        xyConfig["yaxis"]["samples"][0]["axisMin"] = \
            xyConfig["yaxis"]["samples"][0]["ratioMin"]        
        xyConfig["yaxis"]["samples"][0]["axisMax"] = \
            xyConfig["yaxis"]["samples"][0]["ratioMax"]        
        xyConfig["yaxis"]["samples"][0]["axisLabel"] = \
            xyConfig["yaxis"]["samples"][0]["ratioLabel"]

        if "cuts" in xyConfig.keys():
              plotSample(xyConfig,xCutData,[rData],
                         plotUncut=None,pdf_plot=pdf_plot)
        else:
              plotSample(xyConfig,xData,[rData],plotUncut=None,
                         pdf_plot=pdf_plot)
              
    if pdf_plot is not None:
        pdf_plot.close()
        print("Plotted to: " + xyConfig["plot_filename"])

    print("\nThe program has been running for "+
          "{:.2f}".format((time.time()-tic)/60.0)+" minutes....")

    print("End of program")

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-i","--inputFile",dest="inputYaml",type=str,
                        help="insert which YAML file you want to use.")
    parser.add_argument("-o","--overwrite",dest="overwrite",
                        action="store_true",
                        help="Refits the data an overwrites the pickle"
                             " file.")
    parser.add_argument("--linmixerr",dest="linmixerr",
                        action="store_true",
                        help="Uses linmixerr instead of lira to fit"
                             " the data.")
    args = parser.parse_args()
    main()

#########################################################################
# This code simulates what the covariance is when the
# dependent variable is a ratio with the independent variable.
# For Example: fgas vs Mtot.
# This has not been fully integrated into the full program.
'''
def fgas_wl_covariance_mc(xlog,ylog,xlogerr,ylogerr,pdf_plot=None):

    # Step 1, cycle through the different fits and calculate the
    # covariance using a multi-carlo technique.
    # Generate a normal distribution of ylog_err and xlogerr
    nclusters = len(xlog)
    #fgas_wl_covar = fgas_wl_covariance(xlog,xlogerr,ylog)
    fg_log = ylog - xlog
    fg_logerr = np.sqrt(ylogerr**2.0 + xlogerr**2.0)

    for indx in range(nclusters):
        n_mcmc = 1000
        xlog_mcmc = npr.randn(n_mcmc)*xlogerr[indx] + xlog[indx]
        mg_mcmc = npr.randn(n_mcmc)*ylogerr[indx] + ylog[indx]

        # Make a sample of what I think that the simulated data should
        # look like.
        scale_covar = 1.0
        scale_fgas = 1.0#7.58
        new_data = npr.multivariate_normal(
            [xlog[indx],fg_log[indx]], \
            [[xlogerr[indx]**2.0,-scale_covar*xlogerr[indx]**2.0], \
             [-scale_covar*xlogerr[indx]**2.0, \
              (fg_logerr[indx]/scale_fgas)**2.0]],n_mcmc)

        xlim = [min(xlog_mcmc),max(xlog_mcmc)]
        plt.clf()
        plt.subplot(2,2,1)
        plt.scatter(xlog_mcmc,mg_mcmc,marker=".")
        #plt.plot(xlim,
        #        (fgas_wl_covar[indx]*(xlim - xlog[indx]) + \
        #         ylog[indx]/xlog[indx]),"r")
        plt.axvline(xlog[indx])
        plt.axhline(ylog[indx])
        plt.title("Monte Carlo Simulation of the Error")
        plt.xlabel(r"$M_{wl}$")
        plt.ylabel(r"$M_{gas}$")

        plt.subplot(2,2,2)
        plt.hist(xlog_mcmc,histtype="step",label=r"$M_{wl}$",normed=True)
        plt.hist(mg_mcmc,histtype="step",label=r"$M_{gas}$",normed=True)
        plt.hist(mg_mcmc-xlog_mcmc,histtype="step",label=r"$f_{gas}$", \
                 normed=True)
        tempx = np.linspace(-1.5,1.5,100)

        x_scale = 1#60
        y_scale = 1#0
        f_scale = 1#60
        plt.plot(tempx,x_scale*mlab.normpdf(tempx,xlog[indx], \
                                            xlogerr[indx]))
        plt.plot(tempx,y_scale*mlab.normpdf(tempx,ylog[indx], \
                                            ylogerr[indx]))
        plt.plot(tempx,f_scale*mlab.normpdf(tempx,fg_log[indx], \
                                            fg_logerr[indx]))
        plt.legend(fontsize='small',alpha=0.5)

        plt.subplot(2,2,3)
        plt.scatter(xlog_mcmc,mg_mcmc - xlog_mcmc,marker=".")
        plt.scatter(new_data[:,0],new_data[:,1],marker=".",color="r", \
                    alpha=0.5)
        plt.plot(xlim,(-(xlim - xlog[indx]) + ylog[indx] - xlog[indx]), \
                 "r")
        plt.axvline(xlog[indx])
        plt.axhline(ylog[indx] - xlog[indx])

        #plt.title("Monte Carlo Simulation of the Error")
        plt.xlabel(r"$M_{wl}$")
        plt.ylabel(r"$f_{gas}$")

        #plt.subplot(2,2,4)
        if pdf_plot is None:
            plt.show()
        else:
            pdf_plot.savefig()
    return #rho_fgas_mwl
'''
#########################################################################
# This code is not being used currently, keep this until you now to get
# rid of it....
#
# def fgas_wl_covariance(mwl,mwl_err,mgas):#
#
#    # Propagation of errors
#    rho_fgas_mwl = -mwl_err**2*np.abs(mgas)/mwl**2#
#    return rho_fgas_mwl#
#
#########################################################################
