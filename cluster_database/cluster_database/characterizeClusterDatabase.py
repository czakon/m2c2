#!/usr/bin/env python2.7
#########################################################################
# MODIFICATION HISTORY: 
# 05/31/2016: NGC
#
# USAGE:
# ./characterizeClusterDatabase.py
#
# NOTES:
# This differs from stepOneCharacterizeTheData, that program is specific
# to plotting scaling relations, this will plot the data without doing
# any fitting.
#########################################################################
# Standard Python Packages
import sys
import time
import argparse
from code import interact
# Supplemental Python Packages
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from astropy.cosmology.core import FlatLambdaCDM
from astropy import units as u
from astropy.coordinates import SkyCoord
from pymongo import MongoClient
# M2C2 Packages
from load_data.configuration_files import cosmo_config
from load_data.configuration_files import directory_files_config \
    as dir_config
from cluster_database.configuration_files import databaseConfiguration \
    as dbConfig

########################## MAIN PROGRAM ################################

def printDatabaseConfiguration(clusters,argv):

    print('\n' + str(clusters.count()) +
          ' distinct entries in the cluster database.')

    sampleCluster = clusters.find_one()
    references = sampleCluster.keys()
    
    if argv.find_duplicates:

        # Find how many distinct entries are within 1 arcminute of
        # each other, this might indicate that there are duplciates.
        allRa = []
        allDec = []
        allClusterID = []

        print("\nThe first step that we're having problems for is that "
              "there are some entries that seem to be duplicated.")    

        for cluster in clusters.find():
            allClusterID.append(cluster['nominal']['cluster_id'])
            allRa.append(cluster['nominal']['ra'])
            allDec.append(cluster['nominal']['decl'])

        # Find the distances in the catalog with astropy.
        catalog = SkyCoord(ra=allRa*u.degree,dec=allDec*u.degree)
        idx, idx2, d2d, d3d = catalog.search_around_sky(catalog, \
                                                        1*u.arcmin)
        overlap = np.where(idx2!=idx)[0]
        tempX = np.argsort(d2d[overlap])
        overlap = overlap[tempX]

        print("\nThere are " + str(len(overlap)/2) + " entries that "
              "need to be manually checked to see if they're "
              "duplicates.")
                    
        for tX in overlap[0:-1:2]:
            print(allClusterID[idx[tX]] + ' : ' +
                  allClusterID[idx2[tX]] +
                  ' distance: ' + '{:4.2f}'.format(d2d[tX].value*60) +
                  ' arcmins.')
    else:
        print("\nSkipping the find_duplicates diagnostic.")

    interact(local=locals())
    return

def main(argv):

    global tic
    tic = time.time()

    global cosmo
    cosmo = FlatLambdaCDM(H0=cosmo_config.h0*100,Om0=cosmo_config.Omega_M)

    # Initialize the port to communicate with the database
    client = MongoClient(dbConfig.server,dbConfig.port)
    db = client.clusters#Can call db.collection_names
    printDatabaseConfiguration(clusters,argv)
    print('End of printDatabaseConfiguration.')
    interact(local=locals())
    
    plotFilename = dir_config.dbPlotDirectory + 'clusterDatabaseCharacterization.pdf'
    pdf_plot = PdfPages(plotFilename)

    plt.clf()
    plt.rcParams['font.size'] = 30
    plt.figure(figsize=(12,12))
    pdf_plot.savefig()
    pdf_plot.close()

    print('End of program')

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    
    parser.add_argument(
        "-fd","--find_duplicates",action="store_true", \
        help="Identify potential duplicate clusters by seeing if there "
        "are any clusters that are within an arcminute of another.")

    args = parser.parse_args()
    
    main(args)
