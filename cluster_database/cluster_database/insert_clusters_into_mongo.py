#!/usr/bin/env python2.7
##############################################################################
# MODIFICATION HISTORY: 
# 07/20/2015 NGC Created.
# 03/08/2016 NGC Modified extensively to allow me to load the data locally on
#                my computer.
# 05/20/2016 NGC Modified for the public release.
##############################################################################
# STANDARD PYTHON PACKAGES
import time
import argparse
import warnings
from copy import deepcopy
from code import interact
# EXTERNAL PACKAGES
import numpy as np
from astropy.coordinates import SkyCoord
from astropy.cosmology import FlatLambdaCDM
from astropy.io import ascii
from astropy import units as u
from astroquery.ned import Ned
from astroquery.simbad import Simbad
from pymongo import MongoClient
# M2C2 Packages
from configuration_files import databaseConfiguration as dbConfig
from load_data import load_masses,load_xray,load_sze,load_richness
from load_data.configuration_files import cosmo_config

global args, cosmo
cosmo = FlatLambdaCDM(H0=cosmo_config.h0*100,Om0=cosmo_config.Omega_M)
############### CATALOG_TO_JSON ORDER (THIS IS OUT OF DATE) ##################
#MCXC
#BCS
#EBCS
#Reflex
#merten2014
#sereno
#RELICS
#okabe2010
#okabe2015
#hoekstra2015
#umetsu2015
#applegate2014
#hst2015
#mantz2010
#union
#ami
#czakon2015
#mmf3 (not included, and not programmed correctly to be used immediately.
# See the end of this program to find a preleminary template on how to include
# it.
##############################################################################
def simbadQueryObject(clusterRa,clusterDecl,clusterId):

    try:
        if "*" in clusterId:
            if args.verbose:
                print("\nSearching in Simbad on a wildcard...")
            # I don"t really need the warning if Simbad doesn"t like the given
            # cluster name.
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                result = Simbad.query_object(clusterId,wildcard=True)
        else:
            # I don"t really need the warning if Simbad doesn"t like the given
            # cluster name.            
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                result = Simbad.query_object(clusterId)
    except:
        if args.verbose:
            print("No valid simbad query object found...")
        return None

    if len(result)==1:
        
        co = SkyCoord(ra=clusterRa,dec=clusterDecl,unit=(u.deg,u.deg),
                      frame="icrs")
        coResult = SkyCoord(ra=result["RA"][0],dec=result["DEC"][0],
                            unit=(u.hourangle,u.deg),frame="icrs")
        coSep = co.separation(coResult).arcmin
        maxSep = 8.0

        if coSep < maxSep:
            if args.verbose:
                print("\nThe two objects are separated by "
                      + "{:.4f}".format(coSep)
                      + " arcminutes, which is less than "
                      + "{:.4f}".format(maxSep)+", will accept this match.")
        else:
            # If the user selects the no interaction option, we will assume
            # that the error in the distance is in Simbad/Ned
            # (which it usually is).
            #
            if args.noInteract:
                yn = "y"
            else:
                print("Original Search: " + clusterId)
                print("Positive Catalog Match: " + result["MAIN_ID"][0])
                print("Separation: " + "{:.2f}".format(coSep)+" arcminutes")
                yn = raw_input("\nThe objects are located more than "
                               + "{:.2f}".format(maxSep)
                               + " arcminutes from each other, type \"y\" "
                               + "to accept \"n\" to reject: ")
            if yn=="n":
                return None
            
        return result["MAIN_ID"][0]
    
    else:
        print(result)
        print("A search on cluster id should give a unique results.")
        interact(local=locals())
##############################################################################
def simbadQueryRegion(clusterRa,clusterDec):

    result = Simbad.query_criteria("region(circle, "+str(clusterRa)
                                   + " " + str(clusterDecl)+", 2m)",
                                   otype="ClG")
    if len(result)==1:
        return result["MAIN_ID"][0]
    else:
        print(result)
        print("Simbad: Think about what to do here.")
        interact(local=locals())
##############################################################################
def nedQueryObject(clusterId):
    
    try:
        result = Ned.query_object(clusterId)
        nominalId = result["Object Name"][0]

        #result = Simbad.query_object(nominalId)
        #if result == None:
        #    print("\nCould not find the object in Simbad, will use the
        #             Ned default name instead: " + nominalId + "\n")
        #    print([clusterId,nominalId])
        #    return nominalId
        #else:
        #nominalId = result["MAIN_ID"][0]
        if args.verbose:
            print("\nReturning the Simbad identifier")
        return nominalId

    except:
        if args.verbose:
            print("No suitable clusters found in Ned....")
        """
        import difflib        
        if len(goodX[0])==1:
                print(result)
                interaclocal=locals())
                result = result[goodX[0][0]]
                nominalId = result["MAIN_ID"]
            elif len(goodX[0])==0:
                wildCardSearch(clusterId)

                nominalId = None
            else:
                result = result[goodX]
                print("Multiple clusters returned, will make an additional "
                      "cut on redshift...")
                print(result["Object Name"].tolist())
                newResult = result[np.abs(result["Redshift"].data.data 
                                   - clusterZ) < 0.01]
                print("Multiple clusters returned at the same redshift, 
                       will find the one with the best name match....")
                interact(local=locals())
                try:
                    nominalId = difflib.get_close_matches(
                        clusterId,newResult["Object Name"].tolist())[0]
                except:
                    print("No good name matches, will choose the closest"
                          " cluster instead")    
                    goodY = np.argmin(newResult["Distance (arcmin)"])
                    newResult = newResult[goodY]
                    nominalId = newResult["Object Name"]
                print("[catalogId,nedId]")
        """
    #This should be only for searching on coordinates.
    #goodX = np.where(result["Type"]=="GClstr")
    return newClusterId
##############################################################################

def nedQueryRegion(clusterRa,clusterDecl,clusterId):

    co = SkyCoord(ra=clusterRa,dec=clusterDecl,unit=(u.deg,u.deg),frame="icrs")

    try:

        result = Ned.query_region(co,radius=2.0*u.arcmin,equinox="J2000.0")

    except:
        
        if args.verbose:
            print("No appropriate cluster found in the nedQuery region, will"
                  " use the catalogname: " + clusterId)
        return clusterId

    goodX = np.where(result["Type"]=="GClstr")
    result = result[goodX]
    
    if len(goodX[0])==1:
        
        try:

            wildCardSearch(clusterRa,clusterDecl,result["Object Name"][0])

        except:

            if args.verbose:
                print("No valid Simbad object found, will use the Ned name"
                      " instead...")

            nominalId = result["Object Name"][0]
            
        return nominalId
    
    if len(goodX[0])>1:

        # If noInteract flag is sent to the program, it will not choose any of
        # these clusters.
        if args.noInteract:
            input_var = 0
        else:
            print(result)
            queryString = "Please select the cluster number (No.) that " \
                          + "you want to choose for " + clusterId \
                          + " at RA=" + "{:.4f}".format(clusterRa) \
                          + " Dec= "+ "" + "{:.4f}".format(clusterDecl) \
                          + ", 0 for None: "
            input_var = input(queryString)
        try:
            if input_var==0:
                if args.verbose:
                    print("No suitable cluster found in the Simbad query,"
                          " will use the name given in the catalog: " \
                          + clusterId)
                return clusterId
            
            clusterX = np.where(result["No."]==input_var)
            return result[clusterX[0][0]]["Object Name"]
        
        except:
            
            print("Invalid input")
            interact(local=locals())
    else:
        
        if args.verbose:
            print("No suitable cluster found in the Simbad query, will use"
                  " the name given in the catalog: " + clusterId)
        return clusterId
    
    if args.verbose:
        print("\nCould not find the object in Simbad, will use the"
              " Ned default name instead: " + clusterId + "\n")
        
    return clusterId
##############################################################################

def wildCardSearch(clusterRa,clusterDecl,clusterId):
    
    #This function is intested to use the simbad function to do a wild
    #card search if you can"t find the regular cluster.
    
    if " " in clusterId:
        wildCardId = "*" + clusterId.split(" ")
    else:
        wildCardId = raw_input("Please type in the sub-string that you want"
                               " to search for " + clusterId + 
                               ", including \"*\" as a wildcard: ")
        
    return simbadQueryObject(clusterRa,clusterDecl,wildCardId)
##############################################################################

def findAstroqueryMatch(clusterId,clusterRa,clusterDecl,clusterZ=None):

    #First do a search on the cluster id
    originalId = clusterId
    clusterId = clusterId.replace("~"," ")

    if clusterId[0:5] == "Abell":
        clusterId = clusterId.replace("Abell","ACO")

    if clusterId[0:3] == "RXJ":
        clusterId = clusterId.replace("RXJ","RX J")

    filename = "altClusterId.txt"
    newClusterIds = ascii.read(filename,delimiter="\t")
    temp = np.where(newClusterIds["OLD_NAME"]==clusterId)

    if len(temp[0])==1:

        clusterId = newClusterIds["NEW_NAME"][temp].tostring()
        clusterId = clusterId.replace("\x00","")
        
        if args.verbose:
            print("\nSet the name according to the altClusterId file to: " \
                  + clusterId)

    try:

        nominalId = simbadQueryObject(clusterRa,clusterDecl,clusterId)

    except:

        if args.verbose:

            print("\nThe Simbad search was not successful, will try Ned....")

        try:
            
            nominalId = nedQueryObject(clusterId)

        except:

            if args.verbose:

                print("\nNeither the Ned nore the Simbad name search"
                      " worked...will search on coordinates....")

            #If the above doesn"t work, then do a coordinate search
            try:
                
                nominalId = simbadQueryRegion(clusterRa,clusterDecl)

                if nominalId == None:
                    
                    if args.verbose:
                        print("Coordinate search didn\"t work, will try"
                              " wildcard...")
                        
                    nominalId = wildCardSearch(clusterId)
                    
                    if nominalId == None:
                        nominalId = nedQueryRegion(clusterRa,clusterDecl,
                                                   clusterId)
            except:

                nominalId = nedQueryRegion(clusterRa,clusterDecl,clusterId)

    if args.verbose:
        print([originalId,nominalId])

    return nominalId
##############################################################################

def find_matches(clusters,clusterId,clusterRa,clusterDecl,clusterZ=None):

    nominalId = findAstroqueryMatch(clusterId,clusterRa,clusterDecl,
                                    clusterZ=clusterZ)

    if nominalId is not None:

        matches = clusters.find({"nominal.cluster_id":{"$eq":nominalId}})

    else:

        # If we can"t find an entry in Ned, 
        # first search within a 2arcmin box in our database.
        # RA and DEC are in degree integers
        search_radius = 2/60.#*u.deg #5 arcmin
        limits = [clusterRa-search_radius,clusterRa+search_radius, \
                  clusterDecl-search_radius,clusterDecl+search_radius]
        matches = clusters.find({"$and":[{"nominal.decl":{"$lt":limits[3]}},\
                                         {"nominal.decl":{"$gt":limits[2]}},\
                                         {"nominal.ra":{"$gt":limits[0]}},\
                                         {"nominal.ra":{"$lt":limits[1]}}]})

    if matches.count()>1:
        
        print("More than one match, sometimes this happens if the code is"
              " run again after is interrupted.")
        print([clusterId,clusterRa,clusterDecl,clusterZ])
        print([matches[i]["nominal"]["cluster_id"]
               for i in range(matches.count())])
        print([matches[i]["nominal"]["ra"]
               for i in range(matches.count())])
        print([matches[i]["nominal"]["decl"]
               for i in range(matches.count())])
        interact(local=locals())

    return matches,nominalId
##############################################################################

def insert_cluster(clusters,catalog,matches,cluster_j):

    if matches.count() is 0:
        
        nominalId = cluster_j["nominal"]["cluster_id"]
        # If there is no existing entry, then all of the catalog values
        # become the nominal value.
        cluster_j["nominal"] = cluster_j[catalog]
        
        if nominalId is not None:
            cluster_j["nominal"]["cluster_id"] = nominalId
            
        if args.verbose:
            print("\nCreating a new cluster entry for " + catalog + ": " \
                  + cluster_j[catalog]["cluster_id"] + " " \
                  + cluster_j["nominal"]["cluster_id"])
            

        try:
            clusters.insert_one(cluster_j)
        except:
            print("It appears that there is a problem in the format of your "
                  "input data. Maybe this is because the file where the data "
                  "is loaded is not formatted correctly.")
            interact(local=locals())
    else:

        k=0
        # For duplicate matches, first, try to use redshift_cut to seperate
        # them. Then, try to find if one system is more massive than the other,
        # this will be the primary system.
        if matches.count() > 1:

            print ("There are ",matches.count(),
                   " clusters within the search radius.")
            print ("duplicate clusters, will skip for now,",
                   matches[0]["nominal"]["cluster_id"])
            print([matches[i]["nominal"]["cluster_id"]
                   for i in range(matches.count())])
            print([matches[i]["nominal"]["matching_catalog"]
                   for i in range(matches.count())])
            print([matches[i]["nominal"]["ra"]
                   for i in range(matches.count())])
            print([matches[i]["nominal"]["decl"]
                   for i in range(matches.count())])
            print([matches[i]["nominal"]["redshift"]["bf"]
                   for i in range(matches.count())])
            interact(local=locals())

        else:

            # We will simply copy the relevant data to serve as the
            # "nominal data." This isn"t exactly included properly...
            # needs to be fixed.
            cluster_new = deepcopy(matches[0])
            cluster_new[catalog] = cluster_j[catalog]

            # Now, insert the nominal values into the cluster if they are
            # not already set...
            nom_keys = ["mass","mgas","lx","tx"]

            for x_key in nom_keys:
                
                if (x_key in cluster_j[catalog].keys()) \
                    and (x_key in cluster_new["nominal"].keys()):
                    if ("matching_catalog"
                      not in cluster_new["nominal"][x_key].keys()):
                        cluster_new["nominal"][x_key] \
                             = cluster_j[catalog][x_key]
                       
            clusters.find_one_and_replace({"_id":matches[0]["_id"]},
                                          cluster_new)

    return
##############################################################################
# Since this is a large and reliable database, this will be the first step for
# inserting the clusters.
##############################################################################
def insert_catalog(clusters,catalog,data):

    for cluster_j in data:
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"],
            clusterZ=cluster_j[catalog]["redshift"]["bf"])
        
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)
        
    return clusters
############################# MAINPROGRAM ####################################
def main():

    tic = time.time()

    # Initialize the port to communicate with the database
    client = MongoClient(dbConfig.server,dbConfig.port)
    db = client.clusters
    
    db.drop_collection("clusters")

    global clusters
    clusters = db.clusters

    # Count the number of entries
    if clusters.count() > 0:
        print (clusters.count()),"cluster entries in MongoDB \n"

    ##########################################################################
    ## These are the additional catalogs that are included in the database
    ##########################################################################
    
    # The idea is that we insert the clusters in preferential order
    # into the database so that we always a preferred catalog.
    # The Order of this Data determines what the "nominal" value for the
    # cluster is, make sure that you have. A good reason to reorganize them...

    if True:
        
        mX = time.localtime().tm_min
        hX = time.localtime().tm_hour
        nM = (mX + 90)%60
        if mX < 30:
            nH = hX+1
        else:
            nH = hX+2
            
        print("\nInserting the clusters into the database.")
        print("The time is now " + str(hX) + ":" + "{:02d}".format(mX) + ".")
        print("The program will take until approximately "
              + str(nH) + ":" + "{:02d}".format(nM) + " to run.")

        # WL DATA
    if True:
        #Bad
        print "\nInserting sereno (this takes about 10 minutes)"
        clusters = insert_catalog(clusters,"sereno",load_masses.sereno())
    if False:
        #Good
        print "\nInserting applegate2014"
        clusters = insert_catalog(clusters,"applegate2014",
                                  load_masses.applegate2014())
        print "\nInserting umetsu2015"
        clusters = insert_catalog(clusters,"umetsu2015",
                                  load_masses.umetsu2015())        
        print "\nInserting okabe2015"
        clusters = insert_catalog(clusters,"okabe2015",
                                  load_masses.okabe2015())
    if True:
        #Bad
        print "\nInserting merten2015"
        clusters = insert_catalog(clusters,"merten2015",
                                  load_masses.merten2015())
    if False:
        print "\nInserting hoekstra2015"
        clusters = insert_catalog(clusters,"hoekstra2015",
                                  load_masses.hoekstra2015())
        # X-RAY DATA
        print("\ninserting mantz2010 (this takes about 2 minutes)")
        clusters = insert_catalog(clusters,"mantz2010",load_xray.mantz2010())
    if True:        
        print "\nInserting mcxc (~15-24 minutes)"
        clusters = insert_catalog(clusters,"mcxc",load_xray.mcxc())
        print("The program has been running for " \
              + "{:.2f}".format((time.time()-tic)/60.0) + " minutes....")
        print "\nInserting vikhlinin2009 (this takes about 2 minutes)"
        clusters = insert_catalog(clusters,"vikhlinin2009",
                                  load_xray.vikhlinin2009())
        print "\nInserting donahue2015"
        clusters = insert_catalog(clusters,"donahue2015",
                                  load_xray.donahue2015())        
        print "\nInserting landry2013"
        clusters = insert_catalog(clusters,"landry2013",
                                  load_xray.landry2013())
        print "\nInserting bcs (this takes about 3 minutes)"
        clusters = insert_catalog(clusters,"bcs",load_xray.bcs())
        
        # SZE DATA
        
        print "\nInserting czakon2015"
        clusters = insert_catalog(clusters,"czakon2015",
                                  load_sze.czakon2015())
        # Do not include mmf3 explicitly (all clusters in union catalog).
        print "\nInserting planck_union (this takes about 35 minutes)"
        clusters = insert_catalog(clusters,"union",load_sze.planck_union())
        print "\nInserting planck_ami"
        clusters = insert_catalog(clusters,"ami",load_sze.ami())

        # RICHNESS DATA

        print "\nInserting andreon2016"
        clusters = insert_catalog(clusters,"andreon2016",
                                  load_richness.andreon2016())

        # MIXED/OLDER DATA

        print "\nInserting mahdavi2013"
        clusters = insert_catalog(clusters,"mahdavi2013",
                                  load_xray.mahdavi2013())
        print "\nInserting okabe2010"
        clusters = insert_catalog(clusters,"okabe2010",
                                  load_masses.okabe2010())

        print "\nInserting RELICS clusters:"
        clusters = insert_catalog(clusters,"relics",load_masses.relics())
        
        print "\nInserting ebcs"
        clusters = insert_catalog(clusters,"ebcs",load_xray.ebcs())
        
        print "\nInserting reflex (this takes about 6 minutes)"
        clusters = insert_catalog(clusters,"reflex",load_xray.reflex())        

        '''
        print("The program has been running for " \
              + "{:.2f}".format((time.time()-tic)/60.0) + 
              " minutes....")
        print("\nThe program has been running for " \
              + "{:.2f}".format((time.time()-tic)/60.0) + " minutes....\n")
        print("The program has been running for " \
              + "{:.2f}".format((time.time()-tic)/60.0) + " minutes....")
        print("The program has been running for " + \
              "{:.2f}".format((time.time()-tic)/60.0) + " minutes....")
        print ("The program has been running for " + \
              "{:.2f}".format((time.time()-tic)/60.0) + " minutes....")
        '''
    print("\nThe program has been running for a total of: " + \
          "{:.2f}".format((time.time()-tic)/60.0) + " minutes....\n")
    print("End of program")
    print("\a")

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbose",dest="verbose",action="store_true")
    parser.add_argument("-ni","--noInteract",dest="noInteract",
                        action="store_true")
    args = parser.parse_args()
    main()
'''
        #clusters = mantz2010_to_json(clusters)
        #clusters = vikhlinin2009_to_json(clusters)
        #clusters = landry2013_to_json(clusters)
        #clusters = donahue2015_to_json(clusters)
        #clusters = sereno_to_json(clusters)
        #clusters = applegate2014_to_json(clusters)
        #clusters = okabe2015_to_json(clusters)
        #clusters = umetsu2015_to_json(clusters)
        #clusters = merten2015_to_json(clusters)
        #clusters = hoekstra2015_to_json(clusters)
        #clusters = mahdavi2013_to_json(clusters)
        #clusters = czakon2015_to_json(clusters)
        #clusters = planck_union_to_json(clusters) 
        #clusters = ami_to_json(clusters)
        #clusters = relics_to_json(clusters)
        #Then, the richness values.
        #clusters = andreon2016_to_json(clusters)
        #clusters = okabe2010_to_json(clusters)
        #clusters = ebcs_to_json(clusters)
        #clusters = reflex_to_json(clusters)

        #clusters = insert_mcxc(clusters)    

def insert_mcxc(clusters):

    catalog = "mcxc"
    m_x = load_xray.mcxc()
    
    #Add the functionality to search Simbad for the proper identifier

    for cluster_j in m_x:

	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"],
            clusterZ=cluster_j[catalog]["redshift"]["bf"])
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)

    return clusters

        
def bcs_to_json(clusters):

    catalog = "bcs"
    bcs_data = load_xray.bcs()

    for cluster_j in bcs_data:

	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"],
            clusterZ=cluster_j[catalog]["redshift"]["bf"])
        
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)
        
    return clusters
##############################################################################
def ebcs_to_json(clusters):

    catalog = "ebcs"
    bcs_data = load_xray.ebcs()

    for cluster_j in bcs_data:

	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"],
            clusterZ=cluster_j[catalog]["redshift"]["bf"])

        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)

    return clusters
##############################################################################
def donahue2015_to_json(clusters):

    catalog = "donahue2015"
    donahue_x = load_xray.donahue2015()

    for cluster_j in donahue_x:

	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)

    return clusters
##############################################################################
def reflex_to_json(clusters):

    catalog = "reflex"
    reflex_data = load_xray.reflex()
    
    for cluster_j in reflex_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)
        
    return clusters
##############################################################################
def merten2015_to_json(clusters):

    catalog = "merten2015"
    merten_data = load_masses.merten2015()

    for cluster_j in merten_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)
        
    return clusters
##############################################################################
def sereno_to_json(clusters):

    catalog = "sereno"
    sereno_data = load_masses.sereno()

    for cluster_j in sereno_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId        
        insert_cluster(clusters,catalog,matches,cluster_j)

    return clusters
##############################################################################
def relics_to_json(clusters):

    catalog = "relics"
    relics_data = load_masses.relics()

    for cluster_j in relics_data:
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId        
        insert_cluster(clusters,catalog,matches,cluster_j)
        
    return clusters
##############################################################################
def okabe2010_to_json(clusters):

    catalog = "okabe2010"
    okabe2010_data = load_masses.okabe2010()

    for cluster_j in okabe2010_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)    

    return clusters
##############################################################################
def okabe2015_to_json(clusters):

    catalog = "okabe2015"
    locuss_data = load_masses.okabe2015()

    for cluster_j in locuss_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId        
        insert_cluster(clusters,catalog,matches,cluster_j)    
    
    return clusters
##############################################################################
def hoekstra2015_to_json(clusters):
    catalog = "hoekstra2015"
    hoekstra2015_data = load_masses.hoekstra2015()

    for cluster_j in hoekstra2015_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
            
        cluster_j["nominal"]["cluster_id"] = nominalId        
        insert_cluster(clusters,catalog,matches,cluster_j)    

    return clusters
##############################################################################
def umetsu2015_to_json(clusters):

    catalog = "umetsu2015"
    umetsu2015_data = load_masses.umetsu2015()

    for cluster_j in umetsu2015_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId        
        insert_cluster(clusters,catalog,matches,cluster_j)    

    return clusters
##############################################################################
def applegate2014_to_json(clusters):

    catalog = "applegate2014"
    wtg_data = load_masses.applegate2014()

    for cluster_j in wtg_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId        
        insert_cluster(clusters,catalog,matches,cluster_j)    

    return clusters
##############################################################################
def vikhlinin2009_to_json(clusters):

    catalog = "vikhlinin2009"
    v09_data = load_xray.vikhlinin2009()

    for cluster_j in v09_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId        
        insert_cluster(clusters,catalog,matches,cluster_j)    

    return clusters
##############################################################################
def mahdavi2013_to_json(clusters):
    catalog = "mahdavi2013"
    mahdavi_data = load_xray.mahdavi2013()

    for cluster_j in mahdavi_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)    

    return clusters
##############################################################################
def landry2013_to_json(clusters):

    catalog = "landry2013"
    landry_data = load_xray.landry2013()

    for cluster_j in landry_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)    

    return clusters
##############################################################################
def mantz2010_to_json(clusters):

    catalog = "mantz2010"
    mantz_data = load_xray.mantz2010()

    for cluster_j in mantz_data:

	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)    

    return clusters
##############################################################################
def planck_union_to_json(clusters):

    catalog = "union"
    union_data = load_sze.planck_union()

    for cluster_j in union_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)    

    return clusters
##############################################################################
def ami_to_json(clusters):

    catalog = "ami"
    ami_data = load_sze.ami()                                      

    for cluster_j in ami_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)    

    return clusters
##############################################################################
def czakon2015_to_json(clusters):

    catalog = "czakon2015"
    bolo_data = load_sze.czakon2015()

    for cluster_j in bolo_data:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"])
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)    

    return clusters
##############################################################################
def andreon2016_to_json(clusters):
    
    #Don"t have the richness values incorporated into the code yet, this
    #needs to be done in another iteration of the code.
    catalog = "andreon2016"
    andreon_x = load_richness.andreon2016()

    for cluster_j in andreon_x:
        
	matches,nominalId = find_matches(
            clusters,cluster_j[catalog]["alt_cluster_id"],
            cluster_j[catalog]["ra"],cluster_j[catalog]["decl"],
            clusterZ=cluster_j[catalog]["redshift"]["bf"])
        cluster_j["nominal"]["cluster_id"] = nominalId
        insert_cluster(clusters,catalog,matches,cluster_j)

    return clusters
'''
